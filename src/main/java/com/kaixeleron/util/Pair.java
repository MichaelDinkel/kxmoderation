package com.kaixeleron.util;

public record Pair<F, S>(F first, S second) {}
