package com.kaixeleron.util;

public interface Callback<T> {

    void run(T data);

}
