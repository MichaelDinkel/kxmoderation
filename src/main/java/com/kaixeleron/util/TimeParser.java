package com.kaixeleron.util;

import com.kaixeleron.moderation.strings.LocaleManager;

import java.util.concurrent.TimeUnit;

public class TimeParser {

    private static LocaleManager localeManager;

    public static void init(LocaleManager localeManager) {

        TimeParser.localeManager = localeManager;

    }

    private static final long MILLIS_PER_DAY = TimeUnit.DAYS.toMillis(1);
    private static final long MILLIS_PER_HOUR = TimeUnit.HOURS.toMillis(1);
    private static final long MILLIS_PER_MINUTE = TimeUnit.MINUTES.toMillis(1);
    private static final long MILLIS_PER_SECOND = TimeUnit.SECONDS.toMillis(1);

    public static long parseTime(String time) {

        long result = 0L;

        char[] characters = time.toCharArray();
        long current = 0L;

        for (char character : characters) {

            if (Character.isDigit(character)) {

                current *= 10L;
                current += Character.digit(character, 10);

            } else {

                switch (character) {
                    case 's' -> result += TimeUnit.SECONDS.toMillis(current);
                    case 'm' -> result += TimeUnit.MINUTES.toMillis(current);
                    case 'h' -> result += TimeUnit.HOURS.toMillis(current);
                    case 'd' -> result += TimeUnit.DAYS.toMillis(current);
                }

                current = 0L;

            }

        }

        return result;

    }

    public static String timeToString(String locale, long time) {

        long days = time / MILLIS_PER_DAY;
        long hours = (time - (days * MILLIS_PER_DAY)) / MILLIS_PER_HOUR;
        long minutes = (time - ((days * MILLIS_PER_DAY) + (hours * MILLIS_PER_HOUR))) / MILLIS_PER_MINUTE;
        long seconds = (time - ((days * MILLIS_PER_DAY) + (hours * MILLIS_PER_HOUR) + (minutes * MILLIS_PER_MINUTE))) / MILLIS_PER_SECOND;

        StringBuilder timeString = new StringBuilder();

        String dayMessage = localeManager.getMessage(locale, "time.day");
        String hourMessage = localeManager.getMessage(locale, "time.hour");
        String minuteMessage = localeManager.getMessage(locale, "time.minute");
        String secondMessage = localeManager.getMessage(locale, "time.second");
        String pluralCharacter = localeManager.getMessage(locale, "time.plural");
        String andMessage = localeManager.getMessage(locale, "time.and");

        if (dayMessage == null) dayMessage = "";
        if (hourMessage == null) hourMessage = "";
        if (minuteMessage == null) minuteMessage = "";
        if (secondMessage == null) secondMessage = "";
        if (pluralCharacter == null) pluralCharacter = "";
        if (andMessage == null) andMessage = "";

        if (days != 0) timeString.append(days).append(String.format(dayMessage, days == 1 ? "" : pluralCharacter));
        if (hours != 0) timeString.append(hours).append(String.format(hourMessage, hours == 1 ? "" : pluralCharacter));
        if (minutes != 0) timeString.append(minutes).append(String.format(minuteMessage, minutes == 1 ? "" : pluralCharacter));
        if (seconds != 0) timeString.append(seconds).append(String.format(secondMessage, seconds == 1 ? "" : pluralCharacter));

        String result;

        if (timeString.length() > 0) {

            char[] check = new char[2];
            timeString.getChars(timeString.length() - 2, timeString.length(), check, 0);

            if (check[0] == ',' && check[1] == ' ') {

                result = timeString.substring(0, timeString.length() - 2);

            } else {

                result = timeString.toString();

            }

            int commaPos = result.lastIndexOf(',');

            if (commaPos > -1) {

                result = result.substring(0, commaPos) + andMessage + result.substring(commaPos + 1);

            }

        } else {

            result = "0" + String.format(secondMessage, pluralCharacter);

        }

        return result;

    }

}
