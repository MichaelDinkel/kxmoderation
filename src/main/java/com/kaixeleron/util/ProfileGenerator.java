package com.kaixeleron.util;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.RemoteConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.profile.PlayerProfile;

import javax.annotation.Nullable;
import java.util.UUID;

public class ProfileGenerator {

    private ProfileGenerator() {}

    private static final UUID CONSOLE_UUID = UUID.fromString("00000000-0000-1000-0000-000000000000");
    private static final UUID RCON_UUID = UUID.fromString("00000000-0000-1000-0000-000000000001");

    public static @Nullable PlayerProfile getFromSender(CommandSender sender) {

        if (sender instanceof Player player) {

            return player.getPlayerProfile();

        } else if (sender instanceof ConsoleCommandSender) {

            return Bukkit.createPlayerProfile(CONSOLE_UUID, "--CONSOLE--");

        } else if (sender instanceof RemoteConsoleCommandSender) {

            return Bukkit.createPlayerProfile(RCON_UUID, "--RCON--");

        } else {

            return null;

        }

    }

}
