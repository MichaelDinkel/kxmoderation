package com.kaixeleron.util;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.profile.PlayerProfile;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.UUID;

public class OfflinePlayerUtil {

    private static final Gson GSON = new Gson();

    //TODO return on error
    public static void getPlayerData(Plugin main, String name, Callback<PlayerProfile> callback) {

        Player player = Bukkit.getPlayerExact(name);

        if (player != null) {

            callback.run(player.getPlayerProfile());

        } else {

            Bukkit.getScheduler().runTaskAsynchronously(main, () -> {

                String properName = null;
                UUID id = null;
                BufferedReader reader = null;

                try {

                    HttpsURLConnection connection = (HttpsURLConnection) new URL(String.format("https://api.mojang.com/users/profiles/minecraft/%s", name)).openConnection();
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                    StringBuilder response = new StringBuilder();

                    String line = reader.readLine();

                    while (line != null) {

                        response.append(line);
                        line = reader.readLine();

                    }

                    JsonObject responseData = GSON.fromJson(response.toString(), JsonObject.class);

                    properName = responseData.get("name").getAsString();

                    StringBuilder idBuilder = new StringBuilder(responseData.get("id").getAsString());
                    idBuilder.insert(20, "-");
                    idBuilder.insert(16, "-");
                    idBuilder.insert(12, "-");
                    idBuilder.insert(8, "-");

                    id = UUID.fromString(idBuilder.toString());

                } catch (IOException e) {

                    System.err.println("Failed to fetch UUID information from Mojang.");
                    e.printStackTrace();

                } finally {

                    try {

                        if (reader != null) reader.close();

                    } catch (Exception ignored) { }

                }

                final String finalName = properName;
                final UUID finalId = id;
                Bukkit.getScheduler().runTask(main, () -> callback.run(Bukkit.createPlayerProfile(finalId, finalName)));

            });

        }

    }

}
