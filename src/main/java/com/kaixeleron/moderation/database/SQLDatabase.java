package com.kaixeleron.moderation.database;

import com.kaixeleron.moderation.ModerationMain;

import com.kaixeleron.moderation.punishments.*;
import com.kaixeleron.util.Callback;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.bukkit.Bukkit;
import org.bukkit.profile.PlayerProfile;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class SQLDatabase implements ModerationDatabase {

    private boolean failed = false;

    private final ModerationMain main;

    private final HikariDataSource hikari;

    public SQLDatabase(ModerationMain main, SQLConfiguration configuration) {

        this.main = main;

        HikariConfig config = new HikariConfig();
        config.setDriverClassName("com.mysql.jdbc.Driver");
        config.setJdbcUrl(String.format("jdbc:mysql://%s:%d/%s", configuration.address(), configuration.port(), configuration.database()));
        config.setUsername(configuration.username());
        config.setPassword(configuration.password());
        config.setPoolName("[kxModeration-HikariCP]");
        config.setMaximumPoolSize(configuration.maxPoolSize());
        config.setMinimumIdle(configuration.minimumIdle());
        config.setMaxLifetime(configuration.maxLifetime());
        config.setKeepaliveTime(configuration.keepAliveTime());
        config.setConnectionTimeout(configuration.connectionTimeout());
        config.setInitializationFailTimeout(-1L);

        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        config.addDataSourceProperty("useServerPrepStmts", "true");
        config.addDataSourceProperty("useLocalSessionState", "true");
        config.addDataSourceProperty("cacheResultSetMetadata", "true");
        config.addDataSourceProperty("cacheServerConfiguration", "true");
        config.addDataSourceProperty("elideSetAutoCommits", "true");
        config.addDataSourceProperty("maintainTimeStats", "false");
        config.addDataSourceProperty("serverTimezone", "UTC");
        config.addDataSourceProperty("socketTimeout", String.valueOf(TimeUnit.SECONDS.toMillis(30)));

        hikari = new HikariDataSource(config);

        init();

    }

    private void init() {

        try (Connection connection = hikari.getConnection()) {

            try (PreparedStatement create = connection.prepareStatement("CREATE TABLE IF NOT EXISTS `kxmoderation_punishments` (`id` CHAR(20), `type` VARCHAR(7), `uuid` CHAR(36), `name` VARCHAR(16), `reason` VARCHAR(256), `expiry` BIGINT, `punishedByUuid` CHAR(36), `punishedByName` VARCHAR(16), `removed` BIT, `removedBy` VARCHAR(16), `removeReason` VARCHAR(256), PRIMARY KEY(`id`));")) {

                create.executeUpdate();

            }

        } catch (SQLException e) {

            System.err.println("An exception has occurred while attempting to connect to the SQL database.");
            e.printStackTrace();
            failed = true;

        }

    }

    private void writePunishment(Punishment punishment, Callback<Boolean> success) {

        Bukkit.getScheduler().runTaskAsynchronously(main, () -> {

            boolean result = true;

            try (Connection connection = hikari.getConnection()) {

                try (PreparedStatement insert = connection.prepareStatement("INSERT INTO `kxmoderation_punishments` (`id`, `type`, `uuid`, `name`, `reason`, `expiry`, `punishedByUuid`, `punishedByName`, `removed`, `removedBy`, `removeReason`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);")) {

                    insert.setString(1, punishment.getId());
                    insert.setString(2, punishment.getType().name());
                    insert.setString(3, Objects.requireNonNull(punishment.getTarget().getUniqueId()).toString());
                    insert.setString(4, punishment.getTarget().getName());
                    insert.setString(5, punishment.getReason());
                    insert.setLong(6, punishment.getExpiry());
                    insert.setString(7, Objects.requireNonNull(punishment.getPunishedBy().getUniqueId()).toString());
                    insert.setString(8, punishment.getPunishedBy().getName());
                    insert.setBoolean(9, false);
                    insert.setString(10, "");
                    insert.setString(11, "");

                    insert.executeUpdate();

                }

            } catch (SQLException e) {

                result = false;
                e.printStackTrace();

            }

            boolean finalResult = result;
            Bukkit.getScheduler().runTask(main, () -> success.run(finalResult));

        });

    }

    @Override
    public void ban(Ban ban, Callback<Boolean> success) {

        writePunishment(ban, success);

    }

    @Override
    public void mute(Mute mute, Callback<Boolean> success) {

        writePunishment(mute, success);

    }

    @Override
    public void warn(Warning warning, Callback<Boolean> success) {

        writePunishment(warning, success);

    }

    @Override
    public void verbalWarn(Verbal verbal, Callback<Boolean> success) {

        writePunishment(verbal, success);

    }

    private void removePunishment(Punishment.Type type, PlayerProfile profile, String reason, String by, Callback<Boolean> success) {

        Bukkit.getScheduler().runTaskAsynchronously(main, () -> {

            boolean result = true;

            try (Connection connection = hikari.getConnection()) {

                try (PreparedStatement update = connection.prepareStatement("UPDATE `kxmoderation_punishments` SET `removed` = ?, `removedBy` = ?, `removeReason` = ? WHERE `type` = ? AND `uuid` = ? AND `removed` = ?;")) {

                    update.setBoolean(1, true);
                    update.setString(2, by);
                    update.setString(3, reason);
                    update.setString(4, type.name());
                    update.setString(5, Objects.requireNonNull(profile.getUniqueId()).toString());
                    update.setBoolean(6, false);

                    update.executeUpdate();

                }

            } catch (SQLException e) {

                result = false;
                e.printStackTrace();

            }

            boolean finalResult = result;
            Bukkit.getScheduler().runTask(main, () -> success.run(finalResult));

        });

    }

    @Override
    public void unban(PlayerProfile profile, String reason, String by, Callback<Boolean> success) {

        removePunishment(Punishment.Type.BAN, profile, reason, by, success);

    }

    @Override
    public void unmute(PlayerProfile profile, String reason, String by, Callback<Boolean> success) {

        removePunishment(Punishment.Type.MUTE, profile, reason, by, success);

    }

    @Override
    public void unwarn(String punishmentId, String reason, String by, Callback<Boolean> success) {

        Bukkit.getScheduler().runTaskAsynchronously(main, () -> {

            boolean result = true;

            try (Connection connection = hikari.getConnection()) {

                try (PreparedStatement update = connection.prepareStatement("UPDATE `kxmoderation_punishments` SET `removed` = ?, `removedBy` = ?, `removeReason` = ? WHERE `type` = ? AND `id` = ?;")) {

                    update.setBoolean(1, true);
                    update.setString(2, by);
                    update.setString(3, reason);
                    update.setString(4, "WARNING");
                    update.setString(5, punishmentId);

                    update.executeUpdate();

                }

            } catch (SQLException e) {

                result = false;
                e.printStackTrace();

            }

            boolean finalResult = result;
            Bukkit.getScheduler().runTask(main, () -> success.run(finalResult));

        });

    }

    @Override
    public void acknowledgeVerbalWarn(PlayerProfile profile, Callback<Boolean> success) {

        removePunishment(Punishment.Type.VERBAL, profile, "Acknowledged", "-ACKNOWLEDGE-", success);

    }

    @Override
    public void getBan(PlayerProfile profile, Callback<Ban> punishment) {

        Bukkit.getScheduler().runTaskAsynchronously(main, () -> Bukkit.getScheduler().runTask(main, () -> punishment.run(getBanNow(profile))));

    }

    @Override
    public Ban getBanNow(PlayerProfile profile) {

        Ban result = null;

        try (Connection connection = hikari.getConnection()) {

            try (PreparedStatement get = connection.prepareStatement("SELECT * FROM `kxmoderation_punishments` WHERE `type` = ? AND `uuid` = ? AND `removed` = ?;")) {

                get.setString(1, "BAN");
                get.setString(2, Objects.requireNonNull(profile.getUniqueId()).toString());
                get.setBoolean(3, false);

                try (ResultSet rs = get.executeQuery()) {

                    if (rs.next()) {

                        result = new Ban(profile, rs.getString("reason"), rs.getLong("expiry"), Bukkit.createPlayerProfile(UUID.fromString(rs.getString("punishedByUuid")), rs.getString("punishedByName")), rs.getString("id"), rs.getBoolean("removed"));

                    }

                }

            }

        } catch (SQLException e) {

            e.printStackTrace();
            result = new Ban();

        }

        return result;

    }

    @Override
    public void getMute(PlayerProfile profile, Callback<Mute> punishment) {

        Bukkit.getScheduler().runTaskAsynchronously(main, () -> Bukkit.getScheduler().runTask(main, () -> punishment.run(getMuteNow(profile))));

    }

    @Override
    public Mute getMuteNow(PlayerProfile profile) {

        Mute result = null;

        try (Connection connection = hikari.getConnection()) {

            try (PreparedStatement get = connection.prepareStatement("SELECT * FROM `kxmoderation_punishments` WHERE `type` = ? AND `uuid` = ? AND `removed` = ?;")) {

                get.setString(1, "MUTE");
                get.setString(2, Objects.requireNonNull(profile.getUniqueId()).toString());
                get.setBoolean(3, false);

                try (ResultSet rs = get.executeQuery()) {

                    if (rs.next()) {

                        result = new Mute(Bukkit.createPlayerProfile(UUID.fromString(rs.getString("uuid")), rs.getString("name")), rs.getString("reason"), rs.getLong("expiry"), Bukkit.createPlayerProfile(UUID.fromString(rs.getString("punishedByUuid")), rs.getString("punishedByName")), rs.getString("id"), rs.getBoolean("removed"));

                    }

                }

            }

        } catch (SQLException e) {

            e.printStackTrace();
            result = new Mute();

        }

        return result;

    }

    @Override
    public void getWarnings(PlayerProfile profile, Callback<Warning[]> warnings) {

        Bukkit.getScheduler().runTaskAsynchronously(main, () -> {

            List<Warning> result = new ArrayList<>();

            try (Connection connection = hikari.getConnection()) {

                try (PreparedStatement get = connection.prepareStatement("SELECT * FROM `kxmoderation_punishments` WHERE `uuid` = ? AND `type` = ? AND `removed` = ?;")) {

                    get.setString(1, Objects.requireNonNull(profile.getUniqueId()).toString());
                    get.setString(2, "WARNING");
                    get.setBoolean(3, false);

                    try (ResultSet rs = get.executeQuery()) {

                        while (rs.next()) {

                            result.add(new Warning(null, Bukkit.createPlayerProfile(UUID.fromString(rs.getString("uuid")), rs.getString("name")), rs.getString("reason"), Bukkit.createPlayerProfile(UUID.fromString(rs.getString("punishedByUuid")), rs.getString("punishedByName")), rs.getString("id"), rs.getBoolean("removed")));

                        }

                    }

                }

            } catch (SQLException e) {

                e.printStackTrace();
                result.clear();
                result.add(new Warning());

            }

            Bukkit.getScheduler().runTask(main, () -> warnings.run(result.toArray(new Warning[0])));

        });

    }

    @Override
    public void getWarning(String punishmentId, Callback<Warning> warning) {

        Bukkit.getScheduler().runTaskAsynchronously(main, () -> {

            Warning result = null;

            try (Connection connection = hikari.getConnection()) {

                try (PreparedStatement get = connection.prepareStatement("SELECT * FROM `kxmoderation_punishments` WHERE `type` = ? AND `id` = ?;")) {

                    get.setString(1, "WARNING");
                    get.setString(2, punishmentId);

                    try (ResultSet rs = get.executeQuery()) {

                        if (rs.next()) {

                            result = new Warning(null, Bukkit.createPlayerProfile(UUID.fromString(rs.getString("uuid")), rs.getString("name")), rs.getString("reason"), Bukkit.createPlayerProfile(UUID.fromString(rs.getString("punishedByUuid")), rs.getString("punishedByName")), rs.getString("id"), rs.getBoolean("removed"));

                        }

                    }

                }

            } catch (SQLException e) {

                e.printStackTrace();
                result = new Warning();

            }

            Warning finalResult = result;
            Bukkit.getScheduler().runTask(main, () -> warning.run(finalResult));

        });

    }

    @Override
    public void getWarningPoints(PlayerProfile profile, Callback<Integer> points) {

        Bukkit.getScheduler().runTaskAsynchronously(main, () -> {

            int result = -1;

            try (Connection connection = hikari.getConnection()) {

                try (PreparedStatement get = connection.prepareStatement("SELECT `reason` from `kxmoderation_punishments` WHERE `type` = ? AND `uuid` = ? AND `removed` = ?;")) {

                    get.setString(1, "WARNING");
                    get.setString(2, Objects.requireNonNull(profile.getUniqueId()).toString());
                    get.setBoolean(3, false);

                    try (ResultSet rs = get.executeQuery()) {

                        result = 0;

                        while (rs.next()) {

                            result += main.getConfig().getInt(String.format("warningTypes.%s.points", rs.getString("reason"))); // getInt returns 0 if nonexistent

                        }

                    }

                }

            } catch (SQLException e) {

                e.printStackTrace();

            }

            int finalResult = result;
            Bukkit.getScheduler().runTask(main, () -> points.run(finalResult));

        });

    }

    @Override
    public void getVerbalWarning(PlayerProfile profile, Callback<Verbal> verbal) {

        Bukkit.getScheduler().runTaskAsynchronously(main, () -> Bukkit.getScheduler().runTask(main, () -> verbal.run(getVerbalNow(profile))));

    }

    @Override
    public Verbal getVerbalNow(PlayerProfile profile) {

        Verbal result = null;

        try (Connection connection = hikari.getConnection()) {

            try (PreparedStatement get = connection.prepareStatement("SELECT `reason`, `id`, `name` from `kxmoderation_punishments` WHERE `type` = ? AND `uuid` = ? AND `removed` = ?;")) {

                get.setString(1, "VERBAL");
                get.setString(2, Objects.requireNonNull(profile.getUniqueId()).toString());
                get.setBoolean(3, false);

                try (ResultSet rs = get.executeQuery()) {

                    if (rs.next()) {

                        result = new Verbal(profile, rs.getString("reason"), null, rs.getString("id"), false);

                    }

                }

            }

        } catch (SQLException e) {

            e.printStackTrace();
            result = new Verbal(); // Failure

        }

        return result;

    }

    @Override
    public void getPunishmentById(String id, Callback<Punishment> punishment) {

        Bukkit.getScheduler().runTaskAsynchronously(main, () -> {

            Punishment result = null;

            try (Connection connection = hikari.getConnection()) {

                try (PreparedStatement get = connection.prepareStatement("SELECT * FROM `kxmoderation_punishments` WHERE `id` = ?;")) {

                    get.setString(1, id);

                    try (ResultSet rs = get.executeQuery()) {

                        if (rs.next()) {

                            UUID uuid = UUID.fromString(rs.getString("uuid"));
                            String name = rs.getString("name");
                            PlayerProfile profile = Bukkit.createPlayerProfile(uuid, name);
                            String reason = rs.getString("reason");
                            long expiry = rs.getLong("expiry");
                            UUID punishedByUuid = UUID.fromString(rs.getString("punishedBy"));
                            String punishedByName = rs.getString("punishedByName");
                            PlayerProfile punishedBy = Bukkit.createPlayerProfile(punishedByUuid, punishedByName);
                            boolean removed = rs.getBoolean("removed");

                            switch (rs.getString("type")) {
                                case "BAN" -> result = new Ban(profile, reason, expiry, punishedBy, id, removed);
                                case "MUTE" -> result = new Mute(profile, reason, expiry, punishedBy, id, removed);
                                case "WARNING" -> result = new Warning(null, profile, reason, punishedBy, id, removed);
                                case "VERBAL" -> result = new Verbal(profile, reason, punishedBy, id, removed);
                            }

                        }

                    }

                }

            } catch (SQLException e) {

                e.printStackTrace();
                result = new Failure();

            }

            Punishment finalResult = result;
            Bukkit.getScheduler().runTask(main, () -> punishment.run(finalResult));

        });

    }

    @Override
    public boolean hasFailed() {
        
        return failed;
        
    }

    @Override
    public void close() {

        hikari.close();

    }

}
