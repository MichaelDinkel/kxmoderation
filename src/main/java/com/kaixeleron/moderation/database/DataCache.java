package com.kaixeleron.moderation.database;

import com.kaixeleron.moderation.ModerationMain;
import com.kaixeleron.moderation.punishments.Ban;
import com.kaixeleron.moderation.punishments.Mute;
import com.kaixeleron.moderation.punishments.Verbal;
import org.bukkit.Bukkit;
import org.bukkit.profile.PlayerProfile;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class DataCache {

    private final ModerationMain main;
    private final ModerationDatabase database;

    private final Map<UUID, Optional<Ban>> bans;
    private final Map<UUID, Optional<Mute>> mutes;
    private final Map<UUID, Optional<Verbal>> verbals;

    public DataCache(ModerationMain main, ModerationDatabase database) {

        this.main = main;
        this.database = database;
        this.bans = new ConcurrentHashMap<>();
        this.mutes = new ConcurrentHashMap<>();
        this.verbals = new ConcurrentHashMap<>();

    }

    public void setBan(@Nonnull UUID id, @Nullable Ban ban) {

        synchronized (bans) {

            bans.put(id, Optional.ofNullable(ban));

        }

    }

    public void setMute(@Nonnull UUID id, @Nullable Mute mute) {

        mutes.put(id, Optional.ofNullable(mute));

    }

    public void setVerbal(@Nonnull UUID id, @Nullable Verbal verbal) {

        verbals.put(id, Optional.ofNullable(verbal));

    }

    public Optional<Ban> getBan(UUID id) {

        Optional<Ban> ban = bans.get(id);

        if (ban == null) {

            Future<PlayerProfile> futureProfile = Bukkit.getScheduler().callSyncMethod(main, () -> Bukkit.createPlayerProfile(id, null));

            try {

                ban = Optional.ofNullable(database.getBanNow(futureProfile.get()));

            } catch (InterruptedException | ExecutionException ignored) {}

            bans.put(id, ban);

            if (ban != null && ban.isPresent() && ban.get().getExpiry() > 0) {

                Bukkit.getScheduler().runTaskLater(main, () -> bans.remove(id), TimeUnit.MILLISECONDS.toSeconds(ban.get().getExpiry() - System.currentTimeMillis()) * 20L);

            }

        }

        return ban;

    }

    public Optional<Mute> getMute(UUID id) {

        Optional<Mute> mute = mutes.get(id);

        if (mute == null) {

            Future<PlayerProfile> futureProfile = Bukkit.getScheduler().callSyncMethod(main, () -> Bukkit.createPlayerProfile(id, null));

            try {

                mute = Optional.ofNullable(database.getMuteNow(futureProfile.get()));

            } catch (InterruptedException | ExecutionException ignored) {}

            mutes.put(id, mute);

            if (mute != null && mute.isPresent() && mute.get().getExpiry() > 0) {

                Bukkit.getScheduler().runTaskLater(main, () -> mutes.remove(id), TimeUnit.MILLISECONDS.toSeconds(mute.get().getExpiry() - System.currentTimeMillis()) * 20L);

            }

        }

        return mute;

    }

    public Optional<Verbal> getVerbal(UUID id) {

        Optional<Verbal> verbal = verbals.get(id);

        if (verbal == null) {

            Future<PlayerProfile> futureProfile = Bukkit.getScheduler().callSyncMethod(main, () -> Bukkit.createPlayerProfile(id, null));

            try {

                verbal = Optional.ofNullable(database.getVerbalNow(futureProfile.get()));

            } catch (InterruptedException | ExecutionException ignored) {}

            verbals.put(id, verbal);

        }

        return verbal;

    }

    public void clearBan(UUID id) {

        bans.remove(id);

    }

    public void clearMute(UUID id) {

        mutes.remove(id);

    }

    public void clearVerbal(UUID id) {

        verbals.remove(id);

    }

}
