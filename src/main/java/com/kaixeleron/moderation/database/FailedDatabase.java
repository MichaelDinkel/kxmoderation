package com.kaixeleron.moderation.database;

import com.kaixeleron.moderation.punishments.*;
import com.kaixeleron.util.Callback;
import org.bukkit.profile.PlayerProfile;

public class FailedDatabase implements ModerationDatabase {

    @Override
    public void ban(Ban ban, Callback<Boolean> success) {

        success.run(false);

    }

    @Override
    public void mute(Mute mute, Callback<Boolean> success) {

        success.run(false);

    }

    @Override
    public void warn(Warning warning, Callback<Boolean> success) {

        success.run(false);

    }

    @Override
    public void verbalWarn(Verbal verbal, Callback<Boolean> success) {

        success.run(false);

    }

    @Override
    public void unban(PlayerProfile profile, String reason, String by, Callback<Boolean> success) {

        success.run(false);

    }

    @Override
    public void unmute(PlayerProfile profile, String reason, String by, Callback<Boolean> success) {

        success.run(false);

    }

    @Override
    public void unwarn(String punishmentId, String reason, String by, Callback<Boolean> success) {

        success.run(false);

    }

    @Override
    public void acknowledgeVerbalWarn(PlayerProfile profile, Callback<Boolean> success) {

        success.run(false);

    }

    @Override
    public void getBan(PlayerProfile profile, Callback<Ban> punishment) {

        punishment.run(new Ban());

    }

    @Override
    public Ban getBanNow(PlayerProfile profile) {

        return new Ban();

    }

    @Override
    public void getMute(PlayerProfile profile, Callback<Mute> punishment) {

        punishment.run(new Mute());

    }

    @Override
    public Mute getMuteNow(PlayerProfile profile) {

        return new Mute();

    }

    @Override
    public void getWarnings(PlayerProfile profile, Callback<Warning[]> warnings) {

        warnings.run(new Warning[]{ new Warning() });

    }

    @Override
    public void getWarning(String punishmentId, Callback<Warning> warning) {

        warning.run(new Warning());

    }

    @Override
    public void getWarningPoints(PlayerProfile profile, Callback<Integer> points) {

        points.run(-1);

    }

    @Override
    public void getVerbalWarning(PlayerProfile profile, Callback<Verbal> verbal) {

        verbal.run(new Verbal());

    }

    @Override
    public Verbal getVerbalNow(PlayerProfile profile) {

        return new Verbal();

    }

    @Override
    public void getPunishmentById(String id, Callback<Punishment> punishment) {

        punishment.run(new Failure());

    }

    @Override
    public boolean hasFailed() {

        return true;

    }

    @Override
    public void close() {}

}
