package com.kaixeleron.moderation.database;

import com.kaixeleron.moderation.ModerationMain;
import com.kaixeleron.moderation.punishments.*;
import com.kaixeleron.util.Callback;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import org.bukkit.profile.PlayerProfile;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class YAMLDatabase implements ModerationDatabase {

    private final ModerationMain main;

    private final File bans, mutes, warnings, verbalWarnings;
    private FileConfiguration banConfig = null, muteConfig = null, warningConfig = null, verbalWarningConfig = null;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public YAMLDatabase(ModerationMain main) {

        this.main = main;

        File dataFolder = main.getDataFolder();

        dataFolder.mkdir();

        File punishmentsFolder = new File(dataFolder, "punishments");
        punishmentsFolder.mkdir();

        bans = new File(punishmentsFolder, "bans.yml");
        mutes = new File(punishmentsFolder, "mutes.yml");
        warnings = new File(punishmentsFolder, "warnings.yml");
        verbalWarnings = new File(punishmentsFolder, "verbalWarnings.yml");

        reload();

    }

    private void writePunishment(FileConfiguration config, Punishment punishment) {

        String target = Objects.requireNonNull(punishment.getTarget().getUniqueId()).toString();

        config.set(String.format("%s.name", target), punishment.getTarget().getName());
        config.set(String.format("%s.reason", target), punishment.getReason());
        config.set(String.format("%s.expiry", target), punishment.getExpiry());
        config.set(String.format("%s.punishedByUuid", target), Objects.requireNonNull(punishment.getPunishedBy().getUniqueId()).toString());
        config.set(String.format("%s.punishedByName", target), punishment.getPunishedBy().getName());
        config.set(String.format("%s.id", target), punishment.getId());

    }

    @Override
    public void ban(Ban ban, Callback<Boolean> success) {

        writePunishment(banConfig, ban);
        saveBans();

        success.run(true);

    }

    @Override
    public void mute(Mute mute, Callback<Boolean> success) {

        writePunishment(muteConfig, mute);
        saveMutes();

        success.run(true);

    }

    @Override
    public void warn(Warning warning, Callback<Boolean> success) {

        warningConfig.set(String.format("%s.%s.name", warning.getTarget().toString(), warning.getId()), warning.getTarget().getName());
        warningConfig.set(String.format("%s.%s.reason", warning.getTarget().toString(), warning.getId()), warning.getReason());
        warningConfig.set(String.format("%s.%s.expiry", warning.getTarget().toString(), warning.getId()), warning.getExpiry());
        warningConfig.set(String.format("%s.%s.punishedByUuid", warning.getTarget().toString(), warning.getId()), Objects.requireNonNull(warning.getPunishedBy().getUniqueId()).toString());
        warningConfig.set(String.format("%s.%s.punishedByName", warning.getTarget().toString(), warning.getId()), warning.getPunishedBy().getName());

        saveWarnings();

        success.run(true);

    }

    @Override
    public void verbalWarn(Verbal verbal, Callback<Boolean> success) {

        writePunishment(verbalWarningConfig, verbal);
        saveVerbalWarnings();

        success.run(true);

    }

    @Override
    public void unban(PlayerProfile profile, String reason, String by, Callback<Boolean> success) {

        banConfig.set(Objects.requireNonNull(profile.getUniqueId()).toString(), null);
        saveBans();

        success.run(true);

    }

    @Override
    public void unmute(PlayerProfile profile, String reason, String by, Callback<Boolean> success) {

        muteConfig.set(Objects.requireNonNull(profile.getUniqueId()).toString(), null);
        saveMutes();

        success.run(true);

    }

    @Override
    public void unwarn(String punishmentId, String reason, String by, Callback<Boolean> success) {

        Punishment warning = findPunishment(Punishment.Type.WARNING, warningConfig, punishmentId);

        if (warning != null) {

            warningConfig.set(String.format("%s.%s", warning.getTarget().toString(), punishmentId), null);
            saveWarnings();

        }

        success.run(true);

    }

    @Override
    public void acknowledgeVerbalWarn(PlayerProfile profile, Callback<Boolean> success) {

        verbalWarningConfig.set(Objects.requireNonNull(profile.getUniqueId()).toString(), null);
        saveVerbalWarnings();

        success.run(true);

    }

    @Override
    public void getBan(PlayerProfile profile, Callback<Ban> punishment) {

        punishment.run(getBanNow(profile));

    }

    @Override
    public Ban getBanNow(PlayerProfile profile) {

        Ban ban = null;

        ConfigurationSection section = banConfig.getConfigurationSection(Objects.requireNonNull(profile.getUniqueId()).toString());

        if (section != null) {

            ban = new Ban(profile, section.getString("reason"), section.getLong("expiry"), Bukkit.createPlayerProfile(UUID.fromString(Objects.requireNonNull(section.getString("punishedByUuid"))), section.getString("punishedByName")), section.getString("id"), false);

        }

        return ban;

    }

    @Override
    public void getMute(PlayerProfile profile, Callback<Mute> punishment) {

        punishment.run(getMuteNow(profile));

    }

    @Override
    public Mute getMuteNow(PlayerProfile profile) {

        Mute mute = null;

        ConfigurationSection section = muteConfig.getConfigurationSection(Objects.requireNonNull(profile.getUniqueId()).toString());

        if (section != null) {

            mute = new Mute(profile, section.getString("reason"), section.getLong("expiry"), Bukkit.createPlayerProfile(UUID.fromString(Objects.requireNonNull(section.getString("punishedByUuid"))), section.getString("punishedByName")), section.getString("id"), false);

        }

        return mute;

    }

    @Override
    public void getWarnings(PlayerProfile profile, Callback<Warning[]> warnings) {

        Warning[] result;

        ConfigurationSection section = warningConfig.getConfigurationSection(Objects.requireNonNull(profile.getUniqueId()).toString());

        if (section == null) {

            result = new Warning[0];

        } else {

            Set<String> ids = section.getKeys(false);
            result = new Warning[ids.size()];

            int i = 0;

            for (String id : ids) {

                result[i] = new Warning(null, profile, section.getString(String.format("%s.reason", id)), Bukkit.createPlayerProfile(UUID.fromString(Objects.requireNonNull(section.getString("punishedByUuid"))), section.getString("punishedByName")), id, false);
                i++;

            }

        }

        warnings.run(result);

    }

    @Override
    public void getWarning(String punishmentId, Callback<Warning> warning) {

        warning.run((Warning) findPunishment(Punishment.Type.WARNING, warningConfig, punishmentId));

    }

    @Override
    public void getWarningPoints(PlayerProfile profile, Callback<Integer> points) {

        int result = 0;

        ConfigurationSection section = warningConfig.getConfigurationSection(Objects.requireNonNull(profile.getUniqueId()).toString());

        if (section != null) {

            for (String id : section.getKeys(false)) {

                result += main.getConfig().getInt(String.format("warningTypes.%s.points", section.getString(String.format("%s.reason", id)))); // getInt returns 0 if nonexistent

            }

        }

        points.run(result);

    }

    @Override
    public void getVerbalWarning(PlayerProfile profile, Callback<Verbal> verbal) {

        verbal.run(getVerbalNow(profile));

    }

    @Override
    public Verbal getVerbalNow(PlayerProfile profile) {

        Verbal result = null;

        ConfigurationSection section = verbalWarningConfig.getConfigurationSection(Objects.requireNonNull(profile.getUniqueId()).toString());

        if (section != null) {

            result = new Verbal(profile, section.getString("reason"), Bukkit.createPlayerProfile(UUID.fromString(Objects.requireNonNull(section.getString("punishedByUuid"))), section.getString("punishedByName")), section.getString("id"), false);

        }

        return result;

    }

    @Override
    public void getPunishmentById(String id, Callback<Punishment> punishment) {

        Punishment result = findPunishment(Punishment.Type.BAN, banConfig, id);

        if (punishment == null) {

            result = findPunishment(Punishment.Type.MUTE, muteConfig, id);

        }

        if (punishment == null) {

            result = findPunishment(Punishment.Type.WARNING, warningConfig, id);

        }

        if (punishment == null) {

            result = findPunishment(Punishment.Type.VERBAL, verbalWarningConfig, id);

        }

        Objects.requireNonNull(punishment).run(result);

    }

    @Override
    public boolean hasFailed() {

        return false;

    }

    private @Nullable Punishment findPunishment(Punishment.Type type, FileConfiguration data, String id) {

        Punishment punishment = null;

        String[] keys = data.getKeys(false).toArray(new String[0]);
        int i = 0;

        while (punishment == null && i < keys.length) {

            if ((type.equals(Punishment.Type.WARNING) && data.getConfigurationSection(String.format("%s.%s", keys[i], id)) != null) || (!type.equals(Punishment.Type.WARNING) && id.equals(data.getString(String.format("%s.id", keys[i]))))) {

                ConfigurationSection section = type.equals(Punishment.Type.WARNING) ? data.getConfigurationSection(String.format("%s.%s", keys[i], id)) : data.getConfigurationSection(keys[i]);

                if (section != null) {

                    switch (type) {

                        case WARNING -> punishment = new Warning(null, Bukkit.createPlayerProfile(UUID.fromString(keys[i]), section.getString("name")), section.getString("reason"), Bukkit.createPlayerProfile(UUID.fromString(Objects.requireNonNull(section.getString("punishedByUuid"))), section.getString("punishedByName")), id, false);
                        case VERBAL -> punishment = new Verbal(Bukkit.createPlayerProfile(UUID.fromString(keys[i]), section.getString("name")), section.getString("reason"), Bukkit.createPlayerProfile(UUID.fromString(Objects.requireNonNull(section.getString("punishedByUuid"))), section.getString("punishedByName")), section.getString("id"), false);
                        case BAN -> punishment = new Ban(Bukkit.createPlayerProfile(UUID.fromString(keys[i]), section.getString("name")), section.getString("reason"), section.getLong("expiry"), Bukkit.createPlayerProfile(UUID.fromString(Objects.requireNonNull(section.getString("punishedByUuid"))), section.getString("punishedByName")), section.getString("id"), false);
                        case MUTE -> punishment = new Mute(Bukkit.createPlayerProfile(UUID.fromString(keys[i]), section.getString("name")), section.getString("reason"), section.getLong("expiry"), Bukkit.createPlayerProfile(UUID.fromString(Objects.requireNonNull(section.getString("punishedByUuid"))), section.getString("punishedByName")), section.getString("id"), false);

                    }

                }

            }

            i++;

        }

        return punishment;

    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void reload() {

        try {

            bans.createNewFile();
            banConfig = YamlConfiguration.loadConfiguration(bans);

        } catch (IOException e) {

            System.err.println("Could not create bans.yml.");
            e.printStackTrace();

        }

        try {

            mutes.createNewFile();
            muteConfig = YamlConfiguration.loadConfiguration(mutes);

        } catch (IOException e) {

            System.err.println("Could not create mutes.yml.");
            e.printStackTrace();

        }

        try {

            warnings.createNewFile();
            warningConfig = YamlConfiguration.loadConfiguration(warnings);

        } catch (IOException e) {

            System.err.println("Could not create warnings.yml.");
            e.printStackTrace();

        }

        try {

            verbalWarnings.createNewFile();
            verbalWarningConfig = YamlConfiguration.loadConfiguration(verbalWarnings);

        } catch (IOException e) {

            System.err.println("Could not create verbalWarnings.yml.");
            e.printStackTrace();

        }

    }

    private void saveBans() {

        try {

            banConfig.save(bans);

        } catch (IOException e) {

            System.err.println("Could not save bans.yml.");
            e.printStackTrace();

        }

    }

    private void saveMutes() {

        try {

            muteConfig.save(mutes);

        } catch (IOException e) {

            System.err.println("Could not save mutes.yml.");
            e.printStackTrace();

        }

    }

    private void saveWarnings() {

        try {

            warningConfig.save(warnings);

        } catch (IOException e) {

            System.err.println("Could not save warnings.yml.");
            e.printStackTrace();

        }

    }

    private void saveVerbalWarnings() {

        try {

            verbalWarningConfig.save(verbalWarnings);

        } catch (IOException e) {

            System.err.println("Could not save verbalWarnings.yml.");
            e.printStackTrace();

        }

    }

    @Override
    public void close() {
    }

}
