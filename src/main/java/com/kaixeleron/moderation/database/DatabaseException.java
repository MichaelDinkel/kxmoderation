package com.kaixeleron.moderation.database;

public class DatabaseException extends Exception {

    public DatabaseException(Exception parent) {

        super(parent);

    }

}
