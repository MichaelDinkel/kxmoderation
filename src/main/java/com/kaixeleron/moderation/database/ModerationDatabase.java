package com.kaixeleron.moderation.database;

import com.kaixeleron.moderation.punishments.*;
import com.kaixeleron.util.Callback;
import org.bukkit.profile.PlayerProfile;

import java.util.Random;

public interface ModerationDatabase extends AutoCloseable {

    Random RANDOM = new Random();

    void ban(Ban ban, Callback<Boolean> success);

    void mute(Mute mute, Callback<Boolean> success);

    void warn(Warning warning, Callback<Boolean> success);

    void verbalWarn(Verbal verbal, Callback<Boolean> success);

    void unban(PlayerProfile profile, String reason, String by, Callback<Boolean> success);

    void unmute(PlayerProfile profile, String reason, String by, Callback<Boolean> success);

    void unwarn(String punishmentId, String reason, String by, Callback<Boolean> success);

    void acknowledgeVerbalWarn(PlayerProfile profile, Callback<Boolean> success);

    void getBan(PlayerProfile profile, Callback<Ban> punishment);

    Ban getBanNow(PlayerProfile profile);

    void getMute(PlayerProfile profile, Callback<Mute> punishment);

    Mute getMuteNow(PlayerProfile profile);

    void getWarnings(PlayerProfile profile, Callback<Warning[]> warnings);

    void getWarning(String punishmentId, Callback<Warning> warning);

    void getWarningPoints(PlayerProfile profile, Callback<Integer> points);

    void getVerbalWarning(PlayerProfile profile, Callback<Verbal> verbal);

    Verbal getVerbalNow(PlayerProfile profile);

    void getPunishmentById(String id, Callback<Punishment> punishment);

    boolean hasFailed();

    default String generatePunishmentId() {

        return Long.toHexString(RANDOM.nextLong()).toUpperCase();

    }

}
