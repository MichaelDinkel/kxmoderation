package com.kaixeleron.moderation.database;

public record SQLConfiguration(String address, int port, String database, String username, String password, int maxPoolSize, int minimumIdle, int maxLifetime, int keepAliveTime, int connectionTimeout) {}
