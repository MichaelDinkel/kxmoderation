package com.kaixeleron.moderation.discord;

import com.kaixeleron.moderation.ModerationMain;
import com.kaixeleron.moderation.punishments.Punishment;
import com.kaixeleron.util.TimeParser;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import org.bukkit.Bukkit;

import javax.annotation.Nullable;
import javax.security.auth.login.LoginException;

public class DiscordIntegration {

    private final ModerationMain main;
    private @Nullable String token;
    private @Nullable Long channel;
    private @Nullable JDA jda = null;

    public DiscordIntegration(ModerationMain main, @Nullable String token, @Nullable Long channel) {

        this.main = main;
        this.token = token;
        this.channel = channel;
        connect();

    }

    public DiscordIntegration(ModerationMain main) {

        this.main = main;
        this.token = null;
        this.channel = null;

    }

    public void setToken(@Nullable String token) {

        this.token = token;

        if (jda != null) {

            disconnect();

        }

        connect();

    }

    public void setChannel(@Nullable Long channel) {

        this.channel = channel;

    }

    private void connect() {

        try {

            jda = JDABuilder.createDefault(token).build();

        } catch (LoginException e) {

            System.err.println("Failed to connect to Discord.");
            e.printStackTrace();

        }

    }

    public void disconnect() {

        if (jda != null) {

            jda.shutdown();
            jda = null;

        }

    }

    public void postPunishment(Punishment punishment) {

        if (jda != null && channel != null) {

            StringBuilder builder = new StringBuilder();

            builder.append("**User ").append(punishment.getType().getVerb()).append("**\n");
            builder.append("**Username:** ").append(punishment.getTarget().getName()).append("\n");
            builder.append("**UUID:** ").append(punishment.getTarget().getUniqueId()).append("\n");
            if (punishment.getType().equals(Punishment.Type.BAN) || punishment.getType().equals(Punishment.Type.MUTE)) {
                builder.append("**Duration:** ").append(punishment.getExpiry() == -1L ? "permanent" : TimeParser.timeToString("en_us", punishment.getExpiry() - System.currentTimeMillis())).append("\n");
            }
            builder.append("**Reason:** ").append(punishment.getReason()).append("\n");
            builder.append("**Issued by:** ").append(punishment.getPunishedBy()).append("\n");

            final String message = builder.toString();

            Bukkit.getScheduler().runTaskAsynchronously(main, () -> {

                TextChannel textChannel = jda.getTextChannelById(channel);

                if (textChannel != null) {

                    textChannel.sendMessage(message).queue();

                }

            });

        }

    }

    public void postRemoval(Punishment punishment, String reason) {

        if (jda != null && channel != null) {

            final String message = "**Punishment removed**\n" +
                    "**Type:** " + punishment.getType().getFriendlyName() + "\n" +
                    "**Username:** " + punishment.getTarget().getName() + "\n" +
                    "**UUID:** " + punishment.getTarget().getUniqueId() + "\n" +
                    "**Reason:** " + reason + "\n" +
                    "**Removed by:** " + punishment.getPunishedBy() + "\n";

            Bukkit.getScheduler().runTaskAsynchronously(main, () -> {

                TextChannel textChannel = jda.getTextChannelById(channel);

                if (textChannel != null) {

                    textChannel.sendMessage(message).queue();

                }

            });

        }

    }

}
