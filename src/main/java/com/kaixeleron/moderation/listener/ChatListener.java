package com.kaixeleron.moderation.listener;

import com.kaixeleron.moderation.ModerationMain;
import com.kaixeleron.moderation.database.DataCache;
import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.punishments.Mute;
import com.kaixeleron.moderation.punishments.Punishment;
import com.kaixeleron.moderation.punishments.Verbal;
import com.kaixeleron.moderation.strings.LocaleManager;
import com.kaixeleron.util.TimeParser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class ChatListener implements Listener {

    private final ModerationMain main;

    private final LocaleManager localeManager;
    private final ModerationDatabase database;
    private final DataCache cache;

    public ChatListener(ModerationMain main, LocaleManager localeManager, ModerationDatabase database, DataCache cache) {

        this.main = main;

        this.localeManager = localeManager;
        this.database = database;
        this.cache = cache;

    }

    private boolean processMessage(Player player) {

        boolean cancel = false;
        Future<UUID> futureUuid = Bukkit.getScheduler().callSyncMethod(main, player::getUniqueId);

        UUID uuid = null;
        try {
            uuid = futureUuid.get();
        } catch (InterruptedException | ExecutionException ignored) {}

        Optional<Verbal> verbal = cache.getVerbal(uuid);

        if (verbal.isPresent()) {

            if (!verbal.get().getType().equals(Punishment.Type.FAILURE)) {

                Bukkit.getScheduler().runTask(main, () -> {

                    String message = localeManager.getMessage(player.getLocale(), "warning.verbal.mustacknowledge");
                    if (message != null) player.sendMessage(String.format(message, "/acknowledge"));

                });

                cancel = true;

            }

        }

        Optional<Mute> mute = cache.getMute(uuid);

        if (mute.isPresent()) {

            if (mute.get().getType().equals(Punishment.Type.FAILURE)) {

                Future<Boolean> hasBypass = Bukkit.getScheduler().callSyncMethod(main, () -> {

                    boolean bypass = player.hasPermission("kxmoderation.error.bypass");

                    if (!bypass) {

                        String errorMessage = localeManager.getMessage(player.getLocale(), "database.mute.failure");
                        if (errorMessage != null) player.sendMessage(errorMessage);

                    }

                    return bypass;

                });

                try {

                    if (!hasBypass.get()) cancel = true;

                } catch (InterruptedException | ExecutionException ignored) {}

            } else {

                if (mute.get().getExpiry() != -1 && mute.get().getExpiry() < System.currentTimeMillis()) {

                    Bukkit.getScheduler().runTask(main, () -> database.unmute(player.getPlayerProfile(), "Expired", "--EXPIRY--", (ignored) -> {}));

                } else {

                    //TODO this line is slow
                    Future<String> messageFuture = Bukkit.getScheduler().callSyncMethod(main, () -> localeManager.getMessage(player.getLocale(), "command." + (mute.get().getExpiry() == -1 ? "mute" : "tempmute") + ".target"));
                    String message = "";

                    try {

                        message = messageFuture.get();

                    } catch (InterruptedException | ExecutionException ignored) {}

                    if (message != null) {

                        if (mute.get().getExpiry() == -1) {

                            message = String.format(message, mute.get().getReason(), mute.get().getId());

                        } else {

                            message = String.format(message, mute.get().getReason(), TimeParser.timeToString(player.getLocale(), mute.get().getExpiry() - System.currentTimeMillis()), mute.get().getId());

                        }

                    }

                    final String finalMessage = message;
                    Bukkit.getScheduler().runTask(main, () -> player.sendMessage(finalMessage == null ? "" : finalMessage));
                    cancel = true;

                }

            }

        }

        return cancel;

    }

    @EventHandler(priority = EventPriority.LOWEST) // Run first
    public void onChat(final AsyncPlayerChatEvent event) {

        if (processMessage(event.getPlayer())) {

            event.setCancelled(true);

        }

    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onCommand(PlayerCommandPreprocessEvent event) {

        if (main.getConfig().getBoolean("mute-messaging")) {

            String message = event.getMessage();

            // Only execute if the player runs a command with arguments
            if (message.contains(" ")) {

                String command = message.substring(1).split(" ")[0]; // Remove preceding slash and get the command itself

                if (main.getConfig().getStringList("messaging-commands").contains(command)) {

                    if (processMessage(event.getPlayer())) {

                        event.setCancelled(true);

                    }

                }

            }

        }

    }

}
