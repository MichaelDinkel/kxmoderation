package com.kaixeleron.moderation.listener;

import com.kaixeleron.moderation.database.DataCache;
import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.punishments.Ban;
import com.kaixeleron.moderation.punishments.Punishment;
import com.kaixeleron.moderation.punishments.Warning;
import com.kaixeleron.moderation.strings.LocaleManager;
import com.kaixeleron.util.TimeParser;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class JoinListener implements Listener {

    private final LocaleManager localeManager;
    private final ModerationDatabase database;
    private final DataCache cache;

    private final Map<UUID, Ban> loggingIn;
    private final List<UUID> errorMessageReceivers;

    public JoinListener(LocaleManager localeManager, ModerationDatabase database, DataCache cache) {

        this.localeManager = localeManager;
        this.database = database;
        this.cache = cache;
        loggingIn = new ConcurrentHashMap<>();
        errorMessageReceivers = new ArrayList<>();

    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onAsyncLogin(AsyncPlayerPreLoginEvent event) {

        if (event.getLoginResult().equals(AsyncPlayerPreLoginEvent.Result.ALLOWED)) {

            cache.getBan(event.getUniqueId()).ifPresent(value -> loggingIn.put(event.getUniqueId(), value));

        }

    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onAsyncLoginCleanup(AsyncPlayerPreLoginEvent event) {

        if (!event.getLoginResult().equals(AsyncPlayerPreLoginEvent.Result.ALLOWED)) {

            loggingIn.remove(event.getUniqueId());

        }

    }

    @EventHandler(priority = EventPriority.LOW) // Run after permission plugins
    public void onLogin(PlayerLoginEvent event) {

        UUID identifier = event.getPlayer().getUniqueId();

        if (event.getResult().equals(PlayerLoginEvent.Result.ALLOWED)) {

            Ban ban = loggingIn.get(identifier);
            cache.setBan(event.getPlayer().getUniqueId(), ban);

            if (ban != null) {

                if (ban.getType().equals(Punishment.Type.FAILURE)) {

                    boolean bypass = event.getPlayer().hasPermission("kxmoderation.error.bypass");

                    if (bypass) {

                        errorMessageReceivers.add(identifier);

                    } else {

                        String errorMessage = localeManager.getMessage(event.getPlayer().getLocale(), "database.ban.failure");
                        event.disallow(PlayerLoginEvent.Result.KICK_OTHER, Objects.requireNonNullElse(errorMessage, ""));

                    }

                    System.err.println("Could not access the database.");

                } else {

                    if (ban.getExpiry() != -1 && ban.getExpiry() < System.currentTimeMillis()) {

                        database.unban(event.getPlayer().getPlayerProfile(), "Expired", "--EXPIRY--", (ignored) -> {});

                    } else {

                        String message = localeManager.getMessage(event.getPlayer().getLocale(), "command." + (ban.getExpiry() == -1 ? "ban" : "tempban") + ".target");

                        if (message != null) {

                            if (ban.getExpiry() == -1) {

                                message = String.format(message, ban.getReason(), ban.getId());

                            } else {

                                message = String.format(message, ban.getReason(), TimeParser.timeToString(event.getPlayer().getLocale(), ban.getExpiry() - System.currentTimeMillis()), ban.getId());

                            }

                        }

                        event.disallow(PlayerLoginEvent.Result.KICK_BANNED, message == null ? "" : message);

                    }

                }

            }

        }

        loggingIn.remove(identifier);

    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {

        if (event.getPlayer().hasPermission("kxmoderation.error.bypass") && errorMessageReceivers.contains(event.getPlayer().getUniqueId())) {

            String errorMessage = localeManager.getMessage(event.getPlayer().getLocale(), "database.ban.failure");
            if (errorMessage != null) event.getPlayer().sendMessage(errorMessage);

        }

        database.getMute(event.getPlayer().getPlayerProfile(), (mute) -> cache.setMute(event.getPlayer().getUniqueId(), mute));

        database.getVerbalWarning(event.getPlayer().getPlayerProfile(), (verbal) -> {

            if (verbal != null && verbal.getType() != Punishment.Type.FAILURE) {

                verbal.displayMessage(event.getPlayer(), localeManager);
                cache.setVerbal(event.getPlayer().getUniqueId(), verbal);

            }

        });

        database.getWarnings(event.getPlayer().getPlayerProfile(), (warnings) -> {

            for (Warning warning : warnings) {

                if (warning.getExpiry() < System.currentTimeMillis()) {

                    database.unwarn(warning.getId(), "Expired", "--EXPIRY--", (ignored) -> {});

                }

            }

        });

    }

}
