package com.kaixeleron.moderation.command;

import com.kaixeleron.moderation.ModerationMain;
import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.punishments.Warning;
import com.kaixeleron.moderation.strings.LocaleManager;
import com.kaixeleron.util.OfflinePlayerUtil;
import com.kaixeleron.util.TimeParser;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import javax.annotation.Nonnull;

public class CommandCheck implements CommandExecutor {

    private final ModerationMain main;
    private final LocaleManager localeManager;
    private final ModerationDatabase database;

    public CommandCheck(ModerationMain main, LocaleManager localeManager, ModerationDatabase database) {

        this.main = main;
        this.localeManager = localeManager;
        this.database = database;

    }

    @Override
    public boolean onCommand(@Nonnull CommandSender sender, @Nonnull Command cmd, @Nonnull String label, @Nonnull String[] args) {

        if (args.length == 0) {

            sender.sendMessage(cmd.getDescription());
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + cmd.getUsage());

        } else {

            String locale = sender instanceof Player ? ((Player) sender).getLocale() : "en_us";

            OfflinePlayerUtil.getPlayerData(main, args[0], (profile) -> database.getBan(profile, (ban) -> database.getMute(profile, (mute) -> database.getWarnings(profile, (warnings) -> {

                String banned, muted;

                if (ban == null || (ban.getExpiry() != -1L && ban.getExpiry() < System.currentTimeMillis())) {

                    banned = localeManager.getMessage(locale, "command.check.no");

                } else {

                    banned = localeManager.getMessage(locale, "command.check.yes");

                    if (banned != null) {

                        String punishment = localeManager.getMessage(locale, "command.check.punishment");

                        if (punishment != null) {

                            punishment = String.format(punishment, ban.getExpiry() == -1 ? localeManager.getMessage(locale, "command.check.permanent") : TimeParser.timeToString(locale, ban.getExpiry() - System.currentTimeMillis()), ban.getReason(), ban.getPunishedBy().getName(), ban.getId());
                            banned += punishment;

                        }

                    }

                }

                if (mute == null || (mute.getExpiry() != -1L && mute.getExpiry() < System.currentTimeMillis())) {

                    muted = localeManager.getMessage(locale, "command.check.no");

                } else {

                    muted = localeManager.getMessage(locale, "command.check.yes");

                    if (muted != null) {

                        String punishment = localeManager.getMessage(locale, "command.check.punishment");

                        if (punishment != null) {

                            punishment = String.format(punishment, mute.getExpiry() == -1 ? localeManager.getMessage(locale, "command.check.permanent") : TimeParser.timeToString(locale, mute.getExpiry() - System.currentTimeMillis()), mute.getReason(), mute.getPunishedBy().getName(), mute.getId());
                            muted += punishment;

                        }

                    }

                }

                String resultMessage = localeManager.getMessage(locale, "command.check.result");

                if (resultMessage != null) {

                    sender.sendMessage(String.format(resultMessage, args[0], banned, muted));

                }

                String warningsMessage = localeManager.getMessage(locale, "command.check.warnings");

                if (warnings.length == 0) {

                    String noneMessage = localeManager.getMessage(locale, "command.check.none");

                    if (noneMessage != null) {

                        warningsMessage += noneMessage;
                        sender.sendMessage(warningsMessage);

                    }

                } else {

                    if (warningsMessage != null) {

                        sender.sendMessage(warningsMessage);

                    }

                    for (Warning warning : warnings) {

                        TextComponent message = new TextComponent(" - ");
                        message.setColor(net.md_5.bungee.api.ChatColor.YELLOW);

                        TextComponent idButton = new TextComponent(warning.getId());
                        idButton.setColor(net.md_5.bungee.api.ChatColor.AQUA);
                        idButton.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("Left click to copy")));
                        idButton.setClickEvent(new ClickEvent(ClickEvent.Action.COPY_TO_CLIPBOARD, warning.getId()));
                        message.addExtra(idButton);

                        TextComponent colon = new TextComponent(": ");
                        colon.setColor(net.md_5.bungee.api.ChatColor.RED);
                        message.addExtra(colon);

                        TextComponent reason = new TextComponent(warning.getReason());
                        reason.setColor(net.md_5.bungee.api.ChatColor.WHITE);
                        message.addExtra(reason);

                        sender.spigot().sendMessage(message);

                    }

                }

            }))));

        }

        return true;

    }

}
