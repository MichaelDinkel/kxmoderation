package com.kaixeleron.moderation.command;

import com.kaixeleron.moderation.ModerationMain;
import com.kaixeleron.moderation.database.DataCache;
import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.discord.DiscordIntegration;
import com.kaixeleron.moderation.punishments.Punishment;
import com.kaixeleron.moderation.strings.LocaleManager;
import com.kaixeleron.util.OfflinePlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import javax.annotation.Nonnull;

import java.util.Objects;

public class CommandUnmute implements CommandExecutor {

    private final ModerationMain main;
    private final LocaleManager localeManager;
    private final ModerationDatabase database;
    private final DataCache cache;
    private final DiscordIntegration discordIntegration;

    public CommandUnmute(ModerationMain main, LocaleManager localeManager, ModerationDatabase database, DataCache cache, DiscordIntegration discordIntegration) {

        this.main = main;
        this.localeManager = localeManager;
        this.database = database;
        this.cache = cache;
        this.discordIntegration = discordIntegration;

    }

    @Override
    public boolean onCommand(@Nonnull CommandSender sender, @Nonnull Command cmd, @Nonnull String label, @Nonnull String[] args) {

        if (args.length == 0 || args.length == 1) {

            sender.sendMessage(cmd.getDescription());
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + cmd.getUsage());

        } else {

            OfflinePlayerUtil.getPlayerData(main, args[0], (profile) -> {

                cache.clearMute(profile.getUniqueId());

                database.getMute(profile, (mute) -> {

                    if (mute == null) {

                        String existsMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "command.unmute.exists");
                        if (existsMessage != null) sender.sendMessage(String.format(existsMessage, args[0]));

                    } else if (mute.getType().equals(Punishment.Type.FAILURE)) {

                        String errorMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "database.general.failure");
                        if (errorMessage != null) sender.sendMessage(errorMessage);

                    } else {

                        StringBuilder builder = new StringBuilder();

                        for (int i = 1; i < args.length; i++) {

                            builder.append(args[i]).append(" ");

                        }

                        // Remove trailing space
                        String reason = builder.substring(0, builder.length() - 1);

                        database.unmute(profile, reason, sender.getName(), (success) -> {

                            if (success) {

                                discordIntegration.postRemoval(mute, reason);

                                String consoleMessage = localeManager.getMessage("en_us", "command.unmute.broadcast");
                                if (consoleMessage != null) Bukkit.getConsoleSender().sendMessage(String.format(consoleMessage, profile.getName(), sender.getName(), reason));

                                for (Player broadcast : Bukkit.getOnlinePlayers()) {

                                    if (broadcast.hasPermission("kxmoderation.notify.unmute")) {

                                        String broadcastMessage = localeManager.getMessage(broadcast.getLocale(), "command.unmute.broadcast");
                                        if (broadcastMessage != null)
                                            broadcast.sendMessage(String.format(broadcastMessage, profile.getName(), sender.getName(), reason));

                                    }

                                }

                                if (!sender.hasPermission("kxmoderation.notify.unmute")) {

                                    String selfMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "command.unmute.self");
                                    if (selfMessage != null)
                                        sender.sendMessage(String.format(selfMessage, profile.getName(), reason));

                                }

                                Player player = Bukkit.getPlayer(Objects.requireNonNull(profile.getUniqueId()));

                                if (player != null) {

                                    String removedMessage = localeManager.getMessage(player.getLocale(), "command.unmute.target");
                                    if (removedMessage != null) player.sendMessage(String.format(removedMessage, reason));

                                }

                            } else {

                                String errorMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "database.general.failure");
                                if (errorMessage != null) sender.sendMessage(errorMessage);

                            }

                        });

                    }

                });

            });

        }

        return true;

    }

}
