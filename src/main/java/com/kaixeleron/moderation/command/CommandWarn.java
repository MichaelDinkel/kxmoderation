package com.kaixeleron.moderation.command;

import com.kaixeleron.moderation.ModerationMain;
import com.kaixeleron.moderation.database.DataCache;
import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.discord.DiscordIntegration;
import com.kaixeleron.moderation.punishments.Warning;
import com.kaixeleron.moderation.strings.LocaleManager;
import com.kaixeleron.moderation.warnings.WarningPunishmentGenerator;
import com.kaixeleron.util.OfflinePlayerUtil;
import com.kaixeleron.util.ProfileGenerator;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import javax.annotation.Nonnull;

public class CommandWarn implements CommandExecutor {

    private final ModerationMain main;
    private final ModerationDatabase database;
    private final DataCache cache;
    private final LocaleManager localeManager;
    private final WarningPunishmentGenerator generator;
    private final DiscordIntegration discordIntegration;

    public CommandWarn(ModerationMain main, ModerationDatabase database, DataCache cache, LocaleManager localeManager, WarningPunishmentGenerator generator, DiscordIntegration discordIntegration) {

        this.main = main;
        this.database = database;
        this.cache = cache;
        this.localeManager = localeManager;
        this.generator = generator;
        this.discordIntegration = discordIntegration;

    }

    @Override
    public boolean onCommand(@Nonnull CommandSender sender, @Nonnull Command cmd, @Nonnull String label, @Nonnull String[] args) {

        if (args.length == 0 || args.length == 1) {

            sender.sendMessage(cmd.getDescription());
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + cmd.getUsage());
            String reasonsMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "command.warn.reasons");
            if (reasonsMessage != null) sender.sendMessage(String.format(reasonsMessage, generateReasonList()));

        } else {

            if (generator.isValidReason(args[1])) {

                OfflinePlayerUtil.getPlayerData(main, args[0], (profile) -> {

                    final Warning newWarning = new Warning(generator, profile, args[1], ProfileGenerator.getFromSender(sender), database.generatePunishmentId(), false, cache);
                    newWarning.execute(localeManager, database, (success) -> {

                        if (success) {

                            discordIntegration.postPunishment(newWarning);

                            String consoleMessage = localeManager.getMessage("en_us", "command.warn.broadcast");
                            if (consoleMessage != null) Bukkit.getConsoleSender().sendMessage(String.format(consoleMessage, profile.getName(), sender.getName(), args[1].toLowerCase()));

                            for (Player broadcast : Bukkit.getOnlinePlayers()) {

                                if (broadcast.hasPermission("kxmoderation.notify.warn")) {

                                    String broadcastMessage = localeManager.getMessage(broadcast.getLocale(), "command.warn.broadcast");
                                    if (broadcastMessage != null) broadcast.sendMessage(String.format(broadcastMessage, profile.getName(), sender.getName(), args[1].toLowerCase()));

                                }

                            }

                            if (!sender.hasPermission("kxmoderation.notify.warn")) {

                                String selfMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "command.warn.self");
                                if (selfMessage != null) sender.sendMessage(String.format(selfMessage, profile.getName(), args[1].toLowerCase()));

                            }

                        } else {

                            String errorMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "database.general.failure");
                            if (errorMessage != null) sender.sendMessage(errorMessage);

                        }

                    });

                });

            } else {

                String errorMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "command.warn.invalid");
                if (errorMessage != null) sender.sendMessage(String.format(errorMessage, generateReasonList()));

            }

        }

        return true;

    }

    private String generateReasonList() {

        StringBuilder builder = new StringBuilder();

        for (String reason : generator.getValidReasons()) {

            builder.append(ChatColor.WHITE).append(reason).append(ChatColor.RED).append(", ");

        }

        return builder.substring(0, builder.length() - 2) + ".";

    }

}
