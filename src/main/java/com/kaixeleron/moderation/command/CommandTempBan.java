package com.kaixeleron.moderation.command;

import com.kaixeleron.moderation.ModerationMain;
import com.kaixeleron.moderation.database.DataCache;
import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.discord.DiscordIntegration;
import com.kaixeleron.moderation.punishments.Ban;
import com.kaixeleron.moderation.punishments.Punishment;
import com.kaixeleron.moderation.strings.LocaleManager;
import com.kaixeleron.util.OfflinePlayerUtil;
import com.kaixeleron.util.ProfileGenerator;
import com.kaixeleron.util.TimeParser;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import javax.annotation.Nonnull;
import java.util.Objects;

public class CommandTempBan implements CommandExecutor {

    private final ModerationMain main;
    private final LocaleManager localeManager;
    private final ModerationDatabase database;
    private final DataCache cache;
    private final DiscordIntegration discordIntegration;

    public CommandTempBan(ModerationMain main, LocaleManager localeManager, ModerationDatabase database, DataCache cache, DiscordIntegration discordIntegration) {

        this.main = main;
        this.localeManager = localeManager;
        this.database = database;
        this.cache = cache;
        this.discordIntegration = discordIntegration;

    }

    @Override
    public boolean onCommand(@Nonnull CommandSender sender, @Nonnull Command cmd, @Nonnull String label, @Nonnull String[] args) {

        if (args.length == 0 || args.length == 1) {

            sender.sendMessage(cmd.getDescription());
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + cmd.getUsage());

        } else {

            OfflinePlayerUtil.getPlayerData(main, args[0], (profile) -> database.getBan(profile, (ban) -> {

                if (ban != null) {

                    if (ban.getType().equals(Punishment.Type.FAILURE)) {

                        String errorMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "database.general.failure");
                        if (errorMessage != null) sender.sendMessage(errorMessage);

                    } else {

                        String existsMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "command.ban.exists");
                        if (existsMessage != null) sender.sendMessage(String.format(existsMessage, args[0]));

                    }

                } else {

                    long time = -1L;

                    try {

                        time = TimeParser.parseTime(args[1]);

                    } catch (IllegalArgumentException e) {

                        String invalidTimestampMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "command.general.invalidtime");
                        if (invalidTimestampMessage != null)
                            sender.sendMessage(String.format(invalidTimestampMessage, args[1]));

                    }

                    if (time != -1L) {

                        StringBuilder builder = new StringBuilder();

                        for (int i = 2; i < args.length; i++) {

                            builder.append(args[i]).append(" ");

                        }

                        // Remove trailing space
                        String reason = builder.substring(0, builder.length() - 1);

                        String id = database.generatePunishmentId();

                        final Ban newBan = new Ban(profile, reason, System.currentTimeMillis() + time, ProfileGenerator.getFromSender(sender), id, false);
                        newBan.execute(localeManager, database, (success) -> {

                            if (success) {

                                discordIntegration.postPunishment(newBan);
                                cache.setBan(Objects.requireNonNull(profile.getUniqueId()), newBan);

                                String consoleMessage = localeManager.getMessage("en_us", "command.tempban.broadcast");
                                if (consoleMessage != null) Bukkit.getConsoleSender().sendMessage(String.format(consoleMessage, profile.getName(), args[1].toLowerCase(), sender.getName(), reason));

                                for (Player broadcast : Bukkit.getOnlinePlayers()) {

                                    if (broadcast.hasPermission("kxmoderation.notify.tempban")) {

                                        String broadcastMessage = localeManager.getMessage(broadcast.getLocale(), "command.tempban.broadcast");
                                        if (broadcastMessage != null)
                                            broadcast.sendMessage(String.format(broadcastMessage, profile.getName(), args[1].toLowerCase(), sender.getName(), reason));

                                    }

                                }

                                if (!sender.hasPermission("kxmoderation.notify.tempban")) {

                                    String selfMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "command.tempban.self");
                                    if (selfMessage != null)
                                        sender.sendMessage(String.format(selfMessage, profile.getName(), args[1].toLowerCase(), reason));

                                }

                            } else {

                                String errorMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "database.general.failure");
                                if (errorMessage != null) sender.sendMessage(errorMessage);

                            }

                        });

                    }

                }

            }));

        }

        return true;

    }

}
