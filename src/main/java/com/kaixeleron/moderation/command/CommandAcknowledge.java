package com.kaixeleron.moderation.command;

import com.kaixeleron.moderation.database.DataCache;
import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.strings.LocaleManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import javax.annotation.Nonnull;

public class CommandAcknowledge implements CommandExecutor {

    private final ModerationDatabase database;
    private final LocaleManager localeManager;
    private final DataCache cache;

    public CommandAcknowledge(ModerationDatabase database, LocaleManager localeManager, DataCache cache) {

        this.database = database;
        this.localeManager = localeManager;
        this.cache = cache;

    }

    @Override
    public boolean onCommand(@Nonnull CommandSender sender, @Nonnull Command cmd, @Nonnull String label, @Nonnull String[] args) {

        if (sender instanceof Player player) {

            database.getVerbalWarning(player.getPlayerProfile(), (verbal) -> {

                if (verbal == null) {

                    String noWarnMessage = localeManager.getMessage(player.getLocale(), "warning.verbal.none");
                    if (noWarnMessage != null) sender.sendMessage(noWarnMessage);

                } else {

                    database.acknowledgeVerbalWarn(player.getPlayerProfile(), (success) -> {

                        if (success) {

                            String acknowledgeMessage = localeManager.getMessage(player.getLocale(), "warning.verbal.acknowledged");
                            if (acknowledgeMessage != null) sender.sendMessage(acknowledgeMessage);
                            cache.clearVerbal(player.getUniqueId());

                        } else {

                            String errorMessage = localeManager.getMessage(player.getLocale(), "database.general.failure");
                            if (errorMessage != null) sender.sendMessage(errorMessage);

                        }

                    });

                }

            });

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

        return true;

    }

}
