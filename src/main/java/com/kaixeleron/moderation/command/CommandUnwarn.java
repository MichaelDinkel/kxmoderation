package com.kaixeleron.moderation.command;

import com.kaixeleron.moderation.database.DataCache;
import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.discord.DiscordIntegration;
import com.kaixeleron.moderation.punishments.Punishment;
import com.kaixeleron.moderation.strings.LocaleManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import javax.annotation.Nonnull;
import java.util.Objects;

public class CommandUnwarn implements CommandExecutor {

    private final DataCache cache;
    private final ModerationDatabase database;
    private final LocaleManager localeManager;
    private final DiscordIntegration discordIntegration;

    public CommandUnwarn(DataCache cache, ModerationDatabase database, LocaleManager localeManager, DiscordIntegration discordIntegration) {

        this.cache = cache;
        this.database = database;
        this.localeManager = localeManager;
        this.discordIntegration = discordIntegration;

    }

    @Override
    public boolean onCommand(@Nonnull CommandSender sender, @Nonnull Command cmd, @Nonnull String label, @Nonnull String[] args) {

        if (args.length == 0 || args.length == 1) {

            sender.sendMessage(cmd.getDescription());
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + cmd.getUsage());

        } else {

            String punishmentId = args[0].toUpperCase();

            database.getWarning(punishmentId, (warning) -> {

                if (warning == null) {

                    String existsMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "command.unwarn.exists");
                    if (existsMessage != null) sender.sendMessage(String.format(existsMessage, args[0]));

                } else if (warning.getType().equals(Punishment.Type.FAILURE)) {

                    String errorMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "database.general.failure");
                    if (errorMessage != null) sender.sendMessage(errorMessage);

                } else if (warning.isRemoved()) {

                    String removedMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "command.unwarn.expired");
                    if (removedMessage != null) sender.sendMessage(String.format(removedMessage, args[0]));

                } else {

                    StringBuilder reason = new StringBuilder();

                    for (int i = 1; i < args.length; i++) {

                        reason.append(args[i]).append(" ");

                    }

                    database.unwarn(args[0], reason.substring(0, reason.length() - 1), sender.getName(), (success) -> {

                        if (success) {

                            discordIntegration.postRemoval(warning, reason.toString());

                            String consoleMessage = localeManager.getMessage("en_us", "command.unwarn.broadcast");
                            if (consoleMessage != null) Bukkit.getConsoleSender().sendMessage(String.format(consoleMessage, warning.getTarget().getName(), sender.getName(), reason));

                            for (Player broadcast : Bukkit.getOnlinePlayers()) {

                                if (broadcast.hasPermission("kxmoderation.notify.unwarn")) {

                                    String broadcastMessage = localeManager.getMessage(broadcast.getLocale(), "command.unwarn.broadcast");
                                    if (broadcastMessage != null) broadcast.sendMessage(String.format(broadcastMessage, warning.getTarget().getName(), sender.getName(), reason));

                                }

                            }

                            if (!sender.hasPermission("kxmoderation.notify.unwarn")) {

                                String selfMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "command.unwarn.self");
                                if (selfMessage != null) sender.sendMessage(String.format(selfMessage, warning.getTarget().getName(), reason));

                            }

                            database.acknowledgeVerbalWarn(warning.getTarget(), (ignored) -> {});
                            cache.clearVerbal(warning.getTarget().getUniqueId());

                            Player player = Bukkit.getPlayer(Objects.requireNonNull(warning.getTarget().getUniqueId()));

                            if (player != null) {

                                String removedMessage = localeManager.getMessage(player.getLocale(), "command.unwarn.target");
                                if (removedMessage != null) player.sendMessage(String.format(removedMessage, reason));

                            }

                        } else {

                            String errorMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "database.general.failure");
                            if (errorMessage != null) sender.sendMessage(errorMessage);

                        }

                    });

                }

            });

        }

        return true;

    }

}
