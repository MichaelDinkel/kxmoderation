package com.kaixeleron.moderation.command;

import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.strings.LocaleManager;
import com.kaixeleron.util.TimeParser;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import javax.annotation.Nonnull;

public class CommandCheckId implements CommandExecutor {

    private final LocaleManager localeManager;
    private final ModerationDatabase database;

    public CommandCheckId(LocaleManager localeManager, ModerationDatabase database) {

        this.localeManager = localeManager;
        this.database = database;

    }

    @Override
    public boolean onCommand(@Nonnull CommandSender sender, @Nonnull Command cmd, @Nonnull String label, @Nonnull String[] args) {

        if (args.length == 0) {

            sender.sendMessage(cmd.getDescription());
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + cmd.getUsage());

        } else {

            args[0] = args[0].toUpperCase();

            String locale = sender instanceof Player ? ((Player) sender).getLocale() : "en_us";

            database.getPunishmentById(args[0], (punishment) -> {

                if (punishment == null || (punishment.getExpiry() != -1L && punishment.getExpiry() < System.currentTimeMillis())) {

                    String message = localeManager.getMessage(locale, "command.checkid.none");

                    if (message != null) {

                        sender.sendMessage(String.format(message, args[0]));

                    }

                } else {

                    String result = localeManager.getMessage(locale, "command.checkid.result");

                    if (result != null) {

                        String type = localeManager.getMessage(locale, String.format("command.checkid.%s", punishment.getType().name().toLowerCase()));

                        if (type == null) {

                            type = "";

                        }

                        sender.sendMessage(String.format(result, punishment.getId(), type, punishment.getTarget().getName(), punishment.getExpiry() == -1 ? localeManager.getMessage(locale, "command.check.permanent") : TimeParser.timeToString(locale, punishment.getExpiry() - System.currentTimeMillis()), punishment.getReason(), punishment.getPunishedBy()));

                    }

                }

            });

        }

        return true;

    }

}
