package com.kaixeleron.moderation.command;

import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.punishments.Warning;
import com.kaixeleron.moderation.strings.LocaleManager;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import javax.annotation.Nonnull;

public class CommandWarnings implements CommandExecutor {

    private final ModerationDatabase database;
    private final LocaleManager localeManager;

    public CommandWarnings(ModerationDatabase database, LocaleManager localeManager) {

        this.database = database;
        this.localeManager = localeManager;

    }

    @Override
    public boolean onCommand(@Nonnull CommandSender sender, @Nonnull Command cmd, @Nonnull String label, @Nonnull String[] args) {

        if (sender instanceof Player player) {

            database.getWarnings(player.getPlayerProfile(), (warnings) -> {

                if (warnings.length == 0) {

                    String noneMessage = localeManager.getMessage(player.getLocale(), "command.warnings.none");
                    if (noneMessage != null) player.sendMessage(noneMessage);

                } else {

                    String headerMessage = localeManager.getMessage(player.getLocale(), "command.warnings.header");
                    if (headerMessage != null) player.sendMessage(String.format(headerMessage, warnings.length));

                    for (Warning warning : warnings) {

                        TextComponent message = new TextComponent(" - ");
                        message.setColor(net.md_5.bungee.api.ChatColor.YELLOW);

                        TextComponent idButton = new TextComponent(warning.getId());
                        idButton.setColor(net.md_5.bungee.api.ChatColor.AQUA);
                        idButton.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("Left click to copy")));
                        idButton.setClickEvent(new ClickEvent(ClickEvent.Action.COPY_TO_CLIPBOARD,warning.getId()));
                        message.addExtra(idButton);

                        TextComponent colon = new TextComponent(": ");
                        colon.setColor(net.md_5.bungee.api.ChatColor.RED);
                        message.addExtra(colon);

                        TextComponent reason = new TextComponent(warning.getReason());
                        reason.setColor(net.md_5.bungee.api.ChatColor.WHITE);
                        message.addExtra(reason);

                        sender.spigot().sendMessage(message);

                    }

                }

            });

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

        return true;

    }

}
