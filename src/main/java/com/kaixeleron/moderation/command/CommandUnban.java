package com.kaixeleron.moderation.command;

import com.kaixeleron.moderation.ModerationMain;
import com.kaixeleron.moderation.database.DataCache;
import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.discord.DiscordIntegration;
import com.kaixeleron.moderation.punishments.Punishment;
import com.kaixeleron.moderation.strings.LocaleManager;
import com.kaixeleron.util.OfflinePlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import javax.annotation.Nonnull;

public class CommandUnban implements CommandExecutor {

    private final ModerationMain main;
    private final LocaleManager localeManager;
    private final ModerationDatabase database;
    private final DataCache cache;
    private final DiscordIntegration discordIntegration;

    public CommandUnban(ModerationMain main, LocaleManager localeManager, ModerationDatabase database, DataCache cache, DiscordIntegration discordIntegration) {

        this.main = main;
        this.localeManager = localeManager;
        this.database = database;
        this.cache = cache;
        this.discordIntegration = discordIntegration;

    }

    @Override
    public boolean onCommand(@Nonnull CommandSender sender, @Nonnull Command cmd, @Nonnull String label, @Nonnull String[] args) {

        if (args.length == 0 || args.length == 1) {

            sender.sendMessage(cmd.getDescription());
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + cmd.getUsage());

        } else {

            OfflinePlayerUtil.getPlayerData(main, args[0], (profile) -> {

                cache.clearBan(profile.getUniqueId());

                database.getBan(profile, (ban) -> {

                    if (ban == null) {

                        String existsMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "command.unban.exists");
                        if (existsMessage != null) sender.sendMessage(String.format(existsMessage, args[0]));

                    } else if (ban.getType().equals(Punishment.Type.FAILURE)) {

                        String errorMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "database.general.failure");
                        if (errorMessage != null) sender.sendMessage(errorMessage);

                    } else {

                        StringBuilder builder = new StringBuilder();

                        for (int i = 1; i < args.length; i++) {

                            builder.append(args[i]).append(" ");

                        }

                        // Remove trailing space
                        String reason = builder.substring(0, builder.length() - 1);

                        database.unban(profile, reason, sender.getName(), (success) -> {

                            discordIntegration.postRemoval(ban, reason);

                            if (success) {

                                String consoleMessage = localeManager.getMessage("en_us", "command.unban.broadcast");
                                if (consoleMessage != null) Bukkit.getConsoleSender().sendMessage(String.format(consoleMessage, profile.getName(), sender.getName(), reason));

                                for (Player broadcast : Bukkit.getOnlinePlayers()) {

                                    if (broadcast.hasPermission("kxmoderation.notify.unban")) {

                                        String broadcastMessage = localeManager.getMessage(broadcast.getLocale(), "command.unban.broadcast");
                                        if (broadcastMessage != null)
                                            broadcast.sendMessage(String.format(broadcastMessage, profile.getName(), sender.getName(), reason));

                                    }

                                }

                                if (!sender.hasPermission("kxmoderation.notify.unban")) {

                                    String selfMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "command.unban.self");
                                    if (selfMessage != null)
                                        sender.sendMessage(String.format(selfMessage, profile.getName(), reason));

                                }

                            } else {

                                String errorMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "database.general.failure");
                                if (errorMessage != null) sender.sendMessage(errorMessage);

                            }

                        });

                    }

                });

            });

        }

        return true;

    }

}
