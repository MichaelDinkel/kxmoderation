package com.kaixeleron.moderation.command;

import com.kaixeleron.moderation.punishments.Kick;
import com.kaixeleron.moderation.strings.LocaleManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import javax.annotation.Nonnull;

public class CommandKick implements CommandExecutor {

    private final LocaleManager localeManager;

    public CommandKick(LocaleManager localeManager) {

        this.localeManager = localeManager;

    }

    @Override
    public boolean onCommand(@Nonnull CommandSender sender, @Nonnull Command cmd, @Nonnull String label, @Nonnull String[] args) {

        if (args.length == 0 || args.length == 1) {

            sender.sendMessage(cmd.getDescription());
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + cmd.getUsage());

        } else {

            Player player = Bukkit.getPlayerExact(args[0]);

            if (player == null) {

                String notFoundMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "command.general.notfound");
                if (notFoundMessage != null) sender.sendMessage(String.format(notFoundMessage, args[0]));

            } else {

                StringBuilder builder = new StringBuilder();

                for (int i = 1; i < args.length; i++) {

                    builder.append(args[i]).append(" ");

                }

                // Remove trailing space
                String reason = builder.substring(0, builder.length() - 1);

                new Kick(player.getPlayerProfile(), reason, "", null).execute(localeManager, null, (ignored) -> {});

                String consoleMessage = localeManager.getMessage("en_us", "command.kick.broadcast");
                if (consoleMessage != null) Bukkit.getConsoleSender().sendMessage(String.format(consoleMessage, player.getName(), sender.getName(), reason));

                for (Player broadcast : Bukkit.getOnlinePlayers()) {

                    if (broadcast.hasPermission("kxmoderation.notify.kick")) {

                        String broadcastMessage = localeManager.getMessage(broadcast.getLocale(), "command.kick.broadcast");
                        if (broadcastMessage != null) broadcast.sendMessage(String.format(broadcastMessage, player.getName(), sender.getName(), reason));

                    }

                }

                if (!sender.hasPermission("kxmoderation.notify.kick")) {

                    String selfMessage = localeManager.getMessage(sender instanceof Player ? ((Player) sender).getLocale() : "en_us", "command.kick.self");
                    if (selfMessage != null) sender.sendMessage(String.format(selfMessage, player.getName(), reason));

                }

            }

        }

        return true;

    }

}
