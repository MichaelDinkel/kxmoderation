package com.kaixeleron.moderation.strings;

import com.kaixeleron.moderation.ModerationMain;
import org.bukkit.ChatColor;

import org.jetbrains.annotations.Nullable;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class LocaleManager {

    private static final String[] DEFAULT_LOCALES = new String[] { "en_us", "ja_jp" };

    private final ModerationMain main;

    private final Map<String, Locale> locales;

    public LocaleManager(ModerationMain main) {

        this.main = main;

        locales = new HashMap<>();

        File pluginDataFolder = main.getDataFolder();

        //noinspection ResultOfMethodCallIgnored
        pluginDataFolder.mkdir();

        createDefaultFiles(pluginDataFolder);
        loadFiles(new File(pluginDataFolder, "locales"));

    }

    public @Nullable String getMessage(String locale, String key) {

        String result = null;

        Locale data = locales.get(locale);

        if (data != null) {

            result = data.getString(key);

        }

        if (result == null) {

            if (!locale.equals("en_us")) {

                result = getMessage("en_us", key);

            }

        }

        if (result != null) result = ChatColor.translateAlternateColorCodes('&', result);

        return result;

    }

    private void loadFiles(File folder) {

        File[] files = folder.listFiles();

        if (files != null) {

            for (File file : files) {

                String name = file.getName().substring(0, file.getName().length() - 4); // Remove .yml

                try {
                    locales.put(name, Locale.loadLocale(file));
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }

    }

    private void createDefaultFiles(File pluginDataFolder) {

        File folder = new File(pluginDataFolder, "locales");

        //noinspection ResultOfMethodCallIgnored
        folder.mkdir();

        for (String def : DEFAULT_LOCALES) {

            createDefaultFile(folder, def);

        }

        loadFiles(folder);

    }

    private void createDefaultFile(File folder, String name) {

        InputStream stream = main.getResource(String.format("locales/%s.yml", name));

        if (stream != null) {

            try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_16BE))) {

                File file = new File(folder, String.format("%s.yml", name));

                if (file.createNewFile()) {

                    try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, StandardCharsets.UTF_16BE))) {

                        String line = reader.readLine();

                        while (line != null) {

                            writer.write(line);
                            writer.newLine();

                            line = reader.readLine();

                        }

                        writer.flush();

                    } catch (IOException e) {

                        System.err.println("Could not write to new locale file.");
                        e.printStackTrace();

                    }

                }

            } catch (IOException e) {

                System.err.println("Could not create default locale file.");
                e.printStackTrace();

            }

        }

    }

}
