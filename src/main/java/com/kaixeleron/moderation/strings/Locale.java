package com.kaixeleron.moderation.strings;

import com.kaixeleron.util.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Locale {

    private final Map<String, String> strings;

    public static Locale loadLocale(File file) throws IOException {

        return new Locale(file);

    }

    private Locale(File file) throws IOException {

        strings = new ConcurrentHashMap<>();
        loadStrings(file);

    }

    private void loadStrings(File file) throws IOException {

        try (BufferedReader reader = new BufferedReader(new FileReader(file, StandardCharsets.UTF_16BE))) {


            String line = reader.readLine();

            while (line != null) {

                Pair<String, String> string = processLine(line);
                strings.put(string.first(), string.second());

                line = reader.readLine();

            }

        }

    }

    private Pair<String, String> processLine(String line) {

        int separator = line.indexOf(": ");

        char[] key = new char[separator];
        line.getChars(0, separator, key, 0);

        char[] value = new char[line.length() - separator - 2]; // Remove colon and space
        line.getChars(separator + 2, line.length(), value, 0);

        String keyString = new String(key);
        String valueString = new String(value);

        if (valueString.startsWith("\"")) valueString = valueString.substring(1);
        if (valueString.endsWith("\"")) valueString = valueString.substring(0, valueString.length() - 1);

        return new Pair<>(processEscapeCharacters(keyString), processEscapeCharacters(valueString));

    }

    private String processEscapeCharacters(String string) {

        string = string.replace("\\n", "\n");
        string = string.replace("\\t", "\t");
        string = string.replace("\\b", "\b");
        string = string.replace("\\r", "\r");
        string = string.replace("\\f", "\f");
        string = string.replace("\\'", "'");
        string = string.replace("\\\"", "\"");
        string = string.replace("\\\\", "\\");

        return string;

    }

    public String getString(String key) {

        return strings.get(key);

    }

}
