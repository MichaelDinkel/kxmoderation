package com.kaixeleron.moderation.warnings;

import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.punishments.*;
import com.kaixeleron.util.TimeParser;
import org.bukkit.configuration.ConfigurationSection;

import javax.annotation.Nullable;
import java.util.*;

public class WarningPunishmentGenerator {

    private enum WarningActionType {

        VERBAL("verbal", Punishment.Type.VERBAL),
        KICK("kick", Punishment.Type.KICK),
        TEMPMUTE("tempmute", Punishment.Type.MUTE),
        TEMPBAN("tempban", Punishment.Type.BAN),
        MUTE("mute", Punishment.Type.MUTE),
        BAN("ban", Punishment.Type.BAN);

        private final String friendlyName;
        private final Punishment.Type punishmentType;

        WarningActionType(String friendlyName, Punishment.Type punishmentType) {

            this.friendlyName = friendlyName;
            this.punishmentType = punishmentType;

        }

        public String getFriendlyName() {

            return friendlyName;

        }

        public Punishment.Type getPunishmentType() {

            return punishmentType;

        }

        @Nullable
        public static WarningActionType getByFriendlyName(String friendlyName) {

            friendlyName = friendlyName.toLowerCase();

            int i = 0;
            WarningActionType result = null;
            WarningActionType[] types = values();

            while (result == null && i < values().length) {

                if (types[i].getFriendlyName().equalsIgnoreCase(friendlyName)) {

                    result = types[i];

                }

                i++;

            }

            return result;

        }

    }

    private record WarningAction(WarningActionType type, long duration) {}

    private record WarningType(String name, int points, WarningAction[] actions) {}

    private final ModerationDatabase database;

    private final Map<String, WarningType> warningTypes;
    private final TreeMap<Integer, WarningAction[]> warningActions;

    public WarningPunishmentGenerator(ModerationDatabase database, ConfigurationSection warningTypes, ConfigurationSection warningActions) {

        this.database = database;
        this.warningTypes = new HashMap<>();
        this.warningActions = new TreeMap<>();
        load(warningTypes, warningActions);

    }

    public Punishment[] generatePunishments(Warning warning, int points) {

        WarningAction[] actions = warningActions.containsKey(points) ? warningActions.get(points) : warningActions.lastEntry().getValue();
        WarningAction[] specificActions = warningTypes.get(warning.getReason()).actions();

        Punishment[] punishments = new Punishment[actions.length + specificActions.length];

        int i = 0;

        for (; i < actions.length; i++) {

            switch (actions[i].type().getPunishmentType()) {

                case VERBAL -> punishments[i] = new Verbal(warning.getTarget(), warning.getReason(), warning.getPunishedBy(), database.generatePunishmentId(), false);
                case KICK -> punishments[i] = new Kick(warning.getTarget(), warning.getReason(), database.generatePunishmentId(), warning.getPunishedBy());
                case BAN -> punishments[i] = new Ban(warning.getTarget(), warning.getReason(), System.currentTimeMillis() + actions[i].duration(), warning.getPunishedBy(), database.generatePunishmentId(), false);
                case MUTE -> punishments[i] = new Mute(warning.getTarget(), warning.getReason(), System.currentTimeMillis() + actions[i].duration(), warning.getPunishedBy(), database.generatePunishmentId(), false);

            }

        }

        for (; i < specificActions.length + actions.length; i++) {

            switch (specificActions[i - actions.length].type.getPunishmentType()) {

                case VERBAL -> punishments[i] = new Verbal(warning.getTarget(), warning.getReason(), warning.getPunishedBy(), database.generatePunishmentId(), false);
                case KICK -> punishments[i] = new Kick(warning.getTarget(), warning.getReason(), database.generatePunishmentId(), warning.getPunishedBy());
                case BAN -> punishments[i] = new Ban(warning.getTarget(), warning.getReason(), System.currentTimeMillis() + actions[i].duration(), warning.getPunishedBy(), database.generatePunishmentId(), false);
                case MUTE -> punishments[i] = new Mute(warning.getTarget(), warning.getReason(), System.currentTimeMillis() + actions[i].duration(), warning.getPunishedBy(), database.generatePunishmentId(), false);

            }

        }

        return punishments;

    }

    public boolean isValidReason(String reason) {

        return warningTypes.containsKey(reason);

    }

    public String[] getValidReasons() {

        return warningTypes.keySet().toArray(new String[0]);

    }

    public void load(ConfigurationSection warningTypes, ConfigurationSection warningActions) {

        this.warningTypes.clear();
        this.warningActions.clear();

        for (String key : warningTypes.getKeys(false)) {

            this.warningTypes.put(key, new WarningType(key, warningTypes.getInt(String.format("%s.points", key)), parseWarningActions(warningTypes.getStringList(String.format("%s.actions", key)))));

        }

        for (String key : warningActions.getKeys(false)) {

            int points = 0;

            try {

                points = Integer.parseInt(key);

            } catch (NumberFormatException ignored) {}

            if (points <= 0) {

                System.err.printf("Invalid warning point count %s.%n", key);

            } else {

                this.warningActions.put(points, parseWarningActions(warningActions.getStringList(key)));

            }

        }

    }

    @Nullable
    private WarningAction parseWarningAction(String action) {

        WarningAction result = null;

        String[] parts = action.contains(" ") ? action.split(" ") : new String[]{ action };

        switch (parts[0].toLowerCase()) {

            case "verbal":
            case "kick":
            case "mute":
            case "ban":

                result = new WarningAction(WarningActionType.getByFriendlyName(parts[0]), 0L);

                break;

            case "tempmute":
            case "tempban":

                if (parts.length > 1) {

                    long time = TimeParser.parseTime(parts[1]);

                    if (time <= 0L) {

                        System.err.printf("Invalid time on warning action %s.%n", action);

                    } else {

                        result = new WarningAction(WarningActionType.getByFriendlyName(parts[0]), time);

                    }

                } else {

                    System.err.printf("Missing time on warning action %s.%n", action);

                }

                break;

            default:

                System.err.printf("Invalid warning action %s.%n", action);

                break;

        }

        return result;

    }

    private WarningAction[] parseWarningActions(List<String> actionList) {

        WarningAction[] actions = new WarningAction[actionList.size()];

        for (int i = 0; i < actionList.size(); i++) {

            actions[i] = parseWarningAction(actionList.get(i));

        }

        return actions;

    }

}
