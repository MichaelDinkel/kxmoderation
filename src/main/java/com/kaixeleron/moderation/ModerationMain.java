package com.kaixeleron.moderation;

import com.kaixeleron.moderation.command.*;
import com.kaixeleron.moderation.database.*;
import com.kaixeleron.moderation.discord.DiscordIntegration;
import com.kaixeleron.moderation.listener.ChatListener;
import com.kaixeleron.moderation.listener.JoinListener;
import com.kaixeleron.moderation.strings.LocaleManager;
import com.kaixeleron.moderation.warnings.WarningPunishmentGenerator;
import com.kaixeleron.util.TimeParser;
import com.mojang.brigadier.tree.LiteralCommandNode;
import me.lucko.commodore.Commodore;
import me.lucko.commodore.CommodoreProvider;
import me.lucko.commodore.file.CommodoreFileReader;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;

public class ModerationMain extends JavaPlugin {

    private final DiscordIntegration discordIntegration;
    private ModerationDatabase database;

    public ModerationMain() {

        saveDefaultConfig();

        if (getConfig().getBoolean("sql.enabled")) {

            ConfigurationSection sqlSection = getConfig().getConfigurationSection("sql"), poolSection = getConfig().getConfigurationSection("sql.pool");

            if (sqlSection != null && poolSection != null) {

                database = new SQLDatabase(this, new SQLConfiguration(
                        sqlSection.getString("address"),
                        sqlSection.getInt("port"),
                        sqlSection.getString("database"),
                        sqlSection.getString("username"),
                        sqlSection.getString("password"),
                        poolSection.getInt("maximum-size"),
                        poolSection.getInt("minimum-idle"),
                        poolSection.getInt("maximum-lifetime"),
                        poolSection.getInt("keepalive-time"),
                        poolSection.getInt("connection-timeout")
                ));

                if (database.hasFailed()) {

                    database = new FailedDatabase();

                }

            } else {

                database = new FailedDatabase();

            }

        } else {

            database = new YAMLDatabase(this);

        }

        if (getConfig().getBoolean("discordIntegration.enabled")) {

            discordIntegration = new DiscordIntegration(this, getConfig().getString("discordIntegration.token"), getConfig().getLong("discordIntegration.channel"));

        } else {

            discordIntegration = new DiscordIntegration(this);

        }

    }

    @Override
    public void onEnable() {

        saveDefaultConfig();

        DataCache cache = new DataCache(this, database);

        LocaleManager localeManager = new LocaleManager(this);
        TimeParser.init(localeManager);

        WarningPunishmentGenerator warningPunishmentGenerator = new WarningPunishmentGenerator(database, getConfig().getConfigurationSection("warningTypes"), getConfig().getConfigurationSection("warningActions"));

        PluginCommand ban = getCommand("ban");
        PluginCommand check = getCommand("check");
        PluginCommand checkid = getCommand("checkid");
        PluginCommand kick = getCommand("kick");
        PluginCommand mute = getCommand("mute");
        PluginCommand tempban = getCommand("tempban");
        PluginCommand tempmute = getCommand("tempmute");
        PluginCommand unban = getCommand("unban");
        PluginCommand unmute = getCommand("unmute");
        PluginCommand warn = getCommand("warn");
        PluginCommand unwarn = getCommand("unwarn");
        PluginCommand acknowledge = getCommand("acknowledge");
        PluginCommand warnings = getCommand("warnings");

        Commodore commodore = CommodoreProvider.getCommodore(this);

        LiteralCommandNode<?> banNode = null;
        LiteralCommandNode<?> checkNode = null;
        LiteralCommandNode<?> kickNode = null;
        LiteralCommandNode<?> muteNode = null;
        LiteralCommandNode<?> tempbanNode = null;
        LiteralCommandNode<?> tempmuteNode = null;
        LiteralCommandNode<?> unbanNode = null;
        LiteralCommandNode<?> unmuteNode = null;
        LiteralCommandNode<?> warnNode = null;
        LiteralCommandNode<?> unwarnNode = null;
        LiteralCommandNode<?> acknowledgeNode = null;
        LiteralCommandNode<?> warningsNode = null;

        try {

            banNode = CommodoreFileReader.INSTANCE.parse(getResource("ban.commodore"));
            checkNode = CommodoreFileReader.INSTANCE.parse(getResource("check.commodore"));
            kickNode = CommodoreFileReader.INSTANCE.parse(getResource("kick.commodore"));
            muteNode = CommodoreFileReader.INSTANCE.parse(getResource("mute.commodore"));
            tempbanNode = CommodoreFileReader.INSTANCE.parse(getResource("tempban.commodore"));
            tempmuteNode = CommodoreFileReader.INSTANCE.parse(getResource("tempmute.commodore"));
            unbanNode = CommodoreFileReader.INSTANCE.parse(getResource("unban.commodore"));
            unmuteNode = CommodoreFileReader.INSTANCE.parse(getResource("unmute.commodore"));
            warnNode = CommodoreFileReader.INSTANCE.parse(getResource("warn.commodore"));
            unwarnNode = CommodoreFileReader.INSTANCE.parse(getResource("unwarn.commodore"));
            acknowledgeNode = CommodoreFileReader.INSTANCE.parse(getResource("acknowledge.commodore"));
            warningsNode = CommodoreFileReader.INSTANCE.parse(getResource("warnings.commodore"));

        } catch (IOException e) {

            e.printStackTrace();

        }

        if (ban != null) {

            ban.setExecutor(new CommandBan(this, localeManager, database, cache, discordIntegration));
            commodore.register(ban, banNode, player -> player.hasPermission("kxmoderation.command.ban"));

        }

        if (check != null) {

            check.setExecutor(new CommandCheck(this, localeManager, database));
            commodore.register(check, checkNode, player -> player.hasPermission("kxmoderation.command.check"));

        }

        if (checkid != null) {

            checkid.setExecutor(new CommandCheckId(localeManager, database));
            commodore.register(checkid, checkNode, player -> player.hasPermission("kxmoderation.command.checkid"));

        }

        if (kick != null) {

            kick.setExecutor(new CommandKick(localeManager));
            commodore.register(kick, kickNode, player -> player.hasPermission("kxmoderation.command.kick"));

        }

        if (mute != null) {

            mute.setExecutor(new CommandMute(this, localeManager, database, cache, discordIntegration));
            commodore.register(mute, muteNode, player -> player.hasPermission("kxmoderation.command.mute"));

        }

        if (tempban != null) {

            tempban.setExecutor(new CommandTempBan(this, localeManager, database, cache, discordIntegration));
            commodore.register(tempban, tempbanNode, player -> player.hasPermission("kxmoderation.command.tempban"));

        }

        if (tempmute != null) {

            tempmute.setExecutor(new CommandTempMute(this, localeManager, database, cache, discordIntegration));
            commodore.register(tempmute, tempmuteNode, player -> player.hasPermission("kxmoderation.command.tempmute"));

        }

        if (unban != null) {

            unban.setExecutor(new CommandUnban(this, localeManager, database, cache, discordIntegration));
            commodore.register(unban, unbanNode, player -> player.hasPermission("kxmoderation.command.unban"));

        }

        if (unmute != null) {

            unmute.setExecutor(new CommandUnmute(this, localeManager, database, cache, discordIntegration));
            commodore.register(unmute, unmuteNode, player -> player.hasPermission("kxmoderation.command.unmute"));

        }

        if (warn != null) {

            warn.setExecutor(new CommandWarn(this, database, cache, localeManager, warningPunishmentGenerator, discordIntegration));
            commodore.register(warn, warnNode, player -> player.hasPermission("kxmoderation.command.warn"));

        }

        if (unwarn != null) {

            unwarn.setExecutor(new CommandUnwarn(cache, database, localeManager, discordIntegration));
            commodore.register(unwarn, unwarnNode, player -> player.hasPermission("kxmoderation.command.unwarn"));

        }

        if (acknowledge != null) {

            acknowledge.setExecutor(new CommandAcknowledge(database, localeManager, cache));
            commodore.register(acknowledge, acknowledgeNode, player -> player.hasPermission("kxmoderation.command.acknowledge"));

        }

        if (warnings != null) {

            warnings.setExecutor(new CommandWarnings(database, localeManager));
            commodore.register(warnings, warningsNode, player -> player.hasPermission("kxmoderation.command.warnings"));

        }

        getServer().getPluginManager().registerEvents(new JoinListener(localeManager, database, cache), this);
        getServer().getPluginManager().registerEvents(new ChatListener(this, localeManager, database, cache), this);

    }

    @Override
    public void onDisable() {

        try {

            database.close();

        } catch (Exception ignored) {}

    }

}
