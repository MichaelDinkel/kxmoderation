package com.kaixeleron.moderation.punishments;

import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.strings.LocaleManager;
import com.kaixeleron.util.Callback;
import com.kaixeleron.util.TimeParser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.profile.PlayerProfile;

import java.util.Objects;

public class Mute extends Punishment {

    public Mute(PlayerProfile target, String reason, long expiry, PlayerProfile punishedBy, String id, boolean removed) {

        super(Type.MUTE, target, reason, expiry, punishedBy, id, removed);

    }

    public Mute() {

        super(Type.FAILURE);

    }

    @Override
    public void execute(LocaleManager localeManager, ModerationDatabase database, Callback<Boolean> success) {

        if (removed) {

            success.run(false);

        } else {

            database.getMute(target, (mute) -> {

                if (mute == null || mute.getExpiry() < expiry) {

                    database.unmute(target, "--WARNING--", "--WARNING--", (unmuted) -> {

                        if (unmuted) {

                            database.mute(this, (muteSuccess) -> {

                                Player player = Bukkit.getPlayer(Objects.requireNonNull(target.getUniqueId()));

                                if (player != null) {

                                    if (expiry == -1L) {

                                        String muteMessage = localeManager.getMessage(player.getLocale(), "command.mute.target");
                                        if (muteMessage != null) player.sendMessage(String.format(muteMessage, reason, id));

                                    } else {

                                        String tempMuteMessage = localeManager.getMessage(player.getLocale(), "command.tempmute.target");
                                        if (tempMuteMessage != null) player.sendMessage(String.format(tempMuteMessage, reason, TimeParser.timeToString(player.getLocale(), expiry - System.currentTimeMillis()), id));

                                    }

                                }

                                success.run(muteSuccess);

                            });

                        } else {

                            success.run(false);

                        }

                    });

                } else {

                    success.run(false);

                }

            });

        }

    }

}
