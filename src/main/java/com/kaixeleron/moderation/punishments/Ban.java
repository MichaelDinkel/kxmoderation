package com.kaixeleron.moderation.punishments;

import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.strings.LocaleManager;
import com.kaixeleron.util.Callback;
import com.kaixeleron.util.TimeParser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.profile.PlayerProfile;

import java.util.Objects;

public class Ban extends Punishment {

    public Ban(PlayerProfile target, String reason, long expiry, PlayerProfile punishedBy, String id, boolean removed) {

        super(Punishment.Type.BAN, target, reason, expiry, punishedBy, id, removed);

    }

    public Ban() {

        super(Type.FAILURE);

    }

    @Override
    public void execute(LocaleManager localeManager, ModerationDatabase database, Callback<Boolean> success) {

        if (removed) {

            success.run(false);

        } else {

            database.getBan(target, (ban) -> {

                if (ban == null || (ban.getExpiry() < expiry && ban.getExpiry() != -1L)) {

                    database.unban(target, "--WARNING--", "--WARNING--", (unbanned) -> {

                        if (unbanned) {

                            database.ban(this, (banSuccess) -> {

                                Player player = Bukkit.getPlayer(Objects.requireNonNull(target.getUniqueId()));

                                if (player != null) {

                                    if (expiry == -1L) {

                                        String kickMessage = localeManager.getMessage(player.getLocale(), "command.ban.target");
                                        player.kickPlayer(kickMessage == null ? "" : String.format(kickMessage, reason, id));

                                    } else {

                                        String kickMessage = localeManager.getMessage(player.getLocale(), "command.tempban.target");
                                        player.kickPlayer(kickMessage == null ? "" : String.format(kickMessage, reason, TimeParser.timeToString(player.getLocale(), expiry - System.currentTimeMillis()), id));

                                    }

                                }

                                success.run(banSuccess);

                            });

                        } else {

                            success.run(false);

                        }

                    });

                } else {

                    success.run(false);

                }

            });

        }

    }

}
