package com.kaixeleron.moderation.punishments;

import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.strings.LocaleManager;
import com.kaixeleron.util.Callback;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.profile.PlayerProfile;

import java.util.Objects;

public class Kick extends Punishment {

    public Kick(PlayerProfile target, String reason, String id, PlayerProfile punishedBy) {

        super(Type.KICK, target, reason, -1L, punishedBy, id, false);

    }

    @Override
    public void execute(LocaleManager localeManager, ModerationDatabase database, Callback<Boolean> success) {

        Player player = Bukkit.getPlayer(Objects.requireNonNull(target.getUniqueId()));

        if (player != null) {

            String kickMessage = localeManager.getMessage(player.getLocale(), "command.kick.target");
            player.kickPlayer(kickMessage == null ? "" : String.format(kickMessage, reason));

        }

        success.run(true);

    }

}
