package com.kaixeleron.moderation.punishments;

import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.strings.LocaleManager;
import com.kaixeleron.util.Callback;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.profile.PlayerProfile;

import java.util.Objects;

public class Verbal extends Punishment {

    public Verbal(PlayerProfile target, String reason, PlayerProfile punishedBy, String id, boolean removed) {

        super(Type.VERBAL, target, reason, -1L, punishedBy, id, removed);

    }

    public Verbal() {

        super(Type.FAILURE);

    }

    @Override
    public void execute(LocaleManager localeManager, ModerationDatabase database, Callback<Boolean> success) {

        if (removed) {

            success.run(false);

        } else {

            database.verbalWarn(this, (warnSuccess) -> {

                if (warnSuccess) {

                    Player player = Bukkit.getPlayer(Objects.requireNonNull(target.getUniqueId()));

                    if (player != null) {

                        displayMessage(player, localeManager);

                    }

                }

                success.run(warnSuccess);

            });

        }

    }

    public void displayMessage(Player player, LocaleManager localeManager) {

        if (player != null) {

            player.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "--------------------------------------");

            player.sendMessage("");

            String warningMessage = localeManager.getMessage(player.getLocale(), "warning.verbal.received");
            if (warningMessage != null) player.sendMessage(String.format(warningMessage, reason));

            player.sendMessage("");

            String acknowledgeMessage = localeManager.getMessage(player.getLocale(), "warning.verbal.acknowledge");
            if (acknowledgeMessage != null) player.sendMessage(String.format(acknowledgeMessage, "/acknowledge"));

            player.sendMessage("");

            player.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "--------------------------------------");

        }

    }

}
