package com.kaixeleron.moderation.punishments;

import com.kaixeleron.moderation.database.DataCache;
import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.strings.LocaleManager;
import com.kaixeleron.moderation.warnings.WarningPunishmentGenerator;
import com.kaixeleron.util.Callback;
import org.bukkit.profile.PlayerProfile;

import java.util.Objects;

public class Warning extends Punishment {

    private final WarningPunishmentGenerator generator;
    private final DataCache cache;

    public Warning(WarningPunishmentGenerator generator, PlayerProfile target, String reason, PlayerProfile punishedBy, String id, boolean removed, DataCache cache) {

        super(Type.WARNING, target, reason, -1L, punishedBy, id, removed);
        this.generator = generator;
        this.cache = cache;

    }

    public Warning(WarningPunishmentGenerator generator, PlayerProfile target, String reason, PlayerProfile punishedBy, String id, boolean removed) {

        this(generator, target, reason, punishedBy, id, removed, null);

    }

    public Warning() {

        super(Type.FAILURE);
        generator = null;
        cache = null;

    }

    @Override
    public void execute(LocaleManager localeManager, ModerationDatabase database, Callback<Boolean> success) {

        if (removed || generator == null) {

            success.run(false);

        } else {

            database.warn(this, (warnSuccess) -> {

                if (warnSuccess) {

                    database.getWarningPoints(target, (points) -> {

                        Punishment[] punishments = generator.generatePunishments(this, points);

                        for (Punishment punishment : punishments) {

                            punishment.execute(localeManager, database, (ignored) -> {});

                            if (cache != null) {

                                if (punishment instanceof Ban ban) {

                                    cache.setBan(Objects.requireNonNull(target.getUniqueId()), ban);

                                } else if (punishment instanceof Mute mute) {

                                    cache.setMute(Objects.requireNonNull(target.getUniqueId()), mute);

                                } else if (punishment instanceof Verbal verbal) {

                                    cache.setVerbal(Objects.requireNonNull(target.getUniqueId()), verbal);

                                }

                            }

                        }

                    });

                }

                success.run(warnSuccess);

            });

        }

    }

}
