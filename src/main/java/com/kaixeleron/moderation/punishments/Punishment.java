package com.kaixeleron.moderation.punishments;

import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.strings.LocaleManager;
import com.kaixeleron.util.Callback;
import org.bukkit.profile.PlayerProfile;

public abstract class Punishment {

    public enum Type {

        WARNING("Warning", "warned"),
        VERBAL("Verbal warning", "verbally warned"),
        KICK("Kick", "kicked"),
        BAN("Ban", "banned"),
        MUTE("Mute", "muted"),
        FAILURE("", "");

        private final String friendlyName, verb;

        Type(String friendlyName, String verb) {

            this.friendlyName = friendlyName;
            this.verb = verb;

        }

        public String getFriendlyName() {

            return friendlyName;

        }

        public String getVerb() {

            return verb;

        }

    }

    protected final Type type;
    protected final PlayerProfile target;
    protected final String reason;
    protected final String id;
    protected final long expiry;
    protected final PlayerProfile punishedBy;
    protected final boolean removed;

    public Punishment(Type type, PlayerProfile target, String reason, long expiry, PlayerProfile punishedBy, String id, boolean removed) {

        this.type = type;
        this.target = target;
        this.reason = reason;
        this.expiry = expiry;
        this.punishedBy = punishedBy;
        this.id = id;
        this.removed = removed;

    }

    public Punishment(Type type) {

        this(type, null, null, -1L, null, null, false);

    }

    public Type getType() {
        return type;
    }

    public PlayerProfile getTarget() {
        return target;
    }

    public String getReason() {
        return reason;
    }

    public PlayerProfile getPunishedBy() {
        return punishedBy;
    }

    public long getExpiry() {
        return expiry;
    }

    public String getId() {
        return id;
    }

    public boolean isRemoved() {
        return removed;
    }

    public abstract void execute(LocaleManager localeManager, ModerationDatabase database, Callback<Boolean> success);

}
