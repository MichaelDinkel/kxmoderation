package com.kaixeleron.moderation.punishments;

import com.kaixeleron.moderation.database.ModerationDatabase;
import com.kaixeleron.moderation.strings.LocaleManager;
import com.kaixeleron.util.Callback;

public class Failure extends Punishment {

    public Failure() {

        super(Type.FAILURE);

    }

    @Override
    public void execute(LocaleManager localeManager, ModerationDatabase database, Callback<Boolean> success) {}

}
